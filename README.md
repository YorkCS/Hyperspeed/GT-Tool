# GT Tool

## Prereqs

Any modern Linux or Mac running Docker 2018 CE or greater will suffice.

## Running the Tests

```bash
$ docker run -v ${PWD}:/data -w /data --rm mozilla/sbt sbt test
```

## Running the Examples

```bash
$ docker run -v ${PWD}:/data -w /data --rm mozilla/sbt sbt "runMain TreeReduction"
```
