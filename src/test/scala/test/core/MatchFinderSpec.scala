package test.core

import lib.core.common.{E, Function, Graph, Morphism, Rule, V}
import lib.core.matching.MatchFinder
import org.scalatest.flatspec._
import org.scalatest.matchers.must.Matchers

class MatchFinderSpec extends AnyFlatSpec with Matchers {

  it should "find no matches" in {
    val G = Graph(Set(V(1)), Set.empty, Function.empty, Function.empty, Function(Set((V(1), 123))), Function.empty[E, String])

    val r = Rule(
      "r",
      Graph(Set(V(2)), Set.empty, Function.empty, Function.empty, Function(Set((V(2), 42))), Function.empty[E, String]),
      Graph(Set(V(2)), Set.empty, Function.empty, Function.empty, Function(Set((V(2), 42))), Function.empty[E, String]),
      Graph(Set(V(2), V(5)), Set(E(3)), Function(Set((E(3), V(2)))), Function(Set((E(3), V(5)))), Function(Set((V(2), 42), (V(5), 123))), Function(Set((E(3), "foo"))))
    )

    assert(MatchFinder.findMatches(G)(r).isEmpty)
  }

  it should "find two matches" in {
    val G = Graph(Set(V(1), V(11), V(21)), Set.empty, Function.empty, Function.empty, Function(Set((V(1), 42), (V(11), 100), (V(21), 42))), Function.empty[E, String])

    val r = Rule(
      "r",
      Graph(Set(V(2)), Set.empty, Function.empty, Function.empty, Function(Set((V(2), 42))), Function.empty[E, String]),
      Graph(Set(V(2)), Set.empty, Function.empty, Function.empty, Function(Set((V(2), 42))), Function.empty[E, String]),
      Graph(Set(V(2), V(5)), Set(E(3)), Function(Set((E(3), V(2)))), Function(Set((E(3), V(5)))), Function(Set((V(2), 42), (V(5), 123))), Function(Set((E(3), "foo"))))
    )

    val expected = List(
      Morphism(Function(Set((V(2),V(1)))),Function(Set())),
      Morphism(Function(Set((V(2),V(21)))),Function(Set()))
    )

    assert(MatchFinder.findMatches(G)(r).toList == expected)
  }

  it should "find one match" in {
    val G = Graph(Set(V(1), V(2), V(3), V(4), V(5), V(6)), Set(E(1), E(2), E(3), E(4), E(5), E(6)), Function(Set((E(1), V(1)), (E(2), V(3)), (E(3), V(4)), (E(4), V(4)), (E(5), V(5)), (E(6), V(5)))), Function(Set((E(1), V(2)), (E(2), V(4)), (E(3), V(4)), (E(4), V(3)), (E(5), V(6)), (E(6), V(6)))), Function(Set((V(1), 42), (V(2), 42), (V(3), 42), (V(4), 40), (V(5), 42), (V(6), 42))), Function(Set((E(1), "foo"), (E(2), "foo"), (E(3), "bar"), (E(4), "foo"), (E(5), "foo"), (E(6), "foo"))))

    val r = Rule(
      "r",
      Graph(Set(V(10), V(20)), Set(E(30)), Function(Set((E(30), V(10)))), Function(Set((E(30), V(20)))), Function(Set((V(10), 42), (V(20), 42))), Function(Set((E(30), "foo")))),
      Graph(Set.empty, Set.empty, Function.empty, Function.empty, Function.empty[V, Int], Function.empty[E, String]),
      Graph(Set(V(10), V(20)), Set(E(30)), Function(Set((E(30), V(10)))), Function(Set((E(30), V(20)))), Function(Set((V(10), 123), (V(20), 456))), Function(Set((E(30), "bar"))))
    )

    val expected = List(
      Morphism(Function(Set((V(10),V(1)), (V(20),V(2)))),Function(Set((E(30),E(1)))))
    )

    assert(MatchFinder.findMatches(G)(r).toList == expected)
  }

}
