package test.core

import lib.core.common.{E, Function, Graph, Morphism, Rule, V}
import lib.core.matching.MatchApplier
import org.scalatest.flatspec._
import org.scalatest.matchers.must.Matchers

class MatchApplierSpec extends AnyFlatSpec with Matchers {

  it should "apply basic rule" in {
    val G = Graph(Set(V(1), V(11), V(21)), Set.empty, Function.empty, Function.empty, Function(Set((V(1), 42), (V(11), 100), (V(21), 42))), Function.empty[E, String])

    val r = Rule(
      "r",
      Graph(Set(V(2)), Set.empty, Function.empty, Function.empty, Function(Set((V(2), 42))), Function.empty[E, String]),
      Graph(Set(V(2)), Set.empty, Function.empty, Function.empty, Function(Set((V(2), 42))), Function.empty[E, String]),
      Graph(Set(V(2), V(5)), Set(E(3)), Function(Set((E(3), V(2)))), Function(Set((E(3), V(5)))), Function(Set((V(2), 42), (V(5), 123))), Function(Set((E(3), "foo"))))
    )

    val g = Morphism(Function(Set((V(2), V(1)))), Function(Set()))

    val expected = Graph(
      Set(V(2), V(22), V(42), V(11)),
      Set(E(7)), Function(Set((E(7), V(2)))),
      Function(Set((E(7), V(11)))),
      Function(Set((V(2), 42), (V(22), 100), (V(42), 42), (V(11), 123))),
      Function(Set((E(7), "foo")))
    )

    assert(MatchApplier.build(G)(r)(g) == expected)
  }

  it should "apply basic relabelling" in {
    val G = Graph(Set(V(1), V(11), V(21)), Set.empty, Function.empty, Function.empty, Function(Set((V(1), 42), (V(11), 100), (V(21), 42))), Function.empty[E, String])

    val r = Rule(
      "r",
      Graph(Set(V(2)), Set.empty, Function.empty, Function.empty, Function(Set((V(2), 42))), Function.empty[E, String]),
      Graph(Set(V(2)), Set.empty, Function.empty, Function.empty, Function.empty[V, Int], Function.empty[E, String]),
      Graph(Set(V(2), V(5)), Set(E(3)), Function(Set((E(3), V(2)))), Function(Set((E(3), V(5)))), Function(Set((V(2), 12345), (V(5), 123456))), Function(Set((E(3), "bar"))))
    )

    val g = Morphism(Function(Set((V(2), V(21)))), Function(Set()))

    val expected = Graph(
      Set(V(2), V(22), V(42), V(11)),
      Set(E(7)),Function(Set((E(7), V(42)))),
      Function(Set((E(7), V(11)))),
      Function(Set((V(2), 42), (V(22), 100), (V(42), 12345), (V(11), 123456))),
      Function(Set((E(7), "bar")))
    )

    assert(MatchApplier.build(G)(r)(g) == expected)
  }

  it should "track morphism" in {
    val G = Graph(
      Set(V(1), V(2), V(3), V(4), V(5), V(6)),
      Set(E(1), E(2), E(3), E(4), E(5), E(6)),
      Function(Set((E(1), V(1)), (E(2), V(3)), (E(3), V(4)), (E(4), V(4)), (E(5), V(5)), (E(6), V(5)))),
      Function(Set((E(1), V(2)), (E(2), V(4)), (E(3), V(4)), (E(4), V(3)), (E(5), V(6)), (E(6), V(6)))),
      Function(Set((V(1), 42), (V(2), 42), (V(3), 42), (V(4), 40), (V(5), 42), (V(6), 42))),
      Function(Set((E(1), "foo"), (E(2), "foo"), (E(3), "bar"), (E(4), "foo"), (E(5), "foo"), (E(6), "foo")))
    )

    val r = Rule(
      "r",
      Graph(Set(V(10), V(20)), Set(E(30)), Function(Set((E(30), V(10)))), Function(Set((E(30), V(20)))), Function(Set((V(10), 42), (V(20), 42))), Function(Set((E(30), "foo")))),
      Graph(Set.empty, Set.empty, Function.empty, Function.empty, Function.empty[V, Int], Function.empty[E, String]),
      Graph(Set(V(10), V(20)), Set(E(30)), Function(Set((E(30), V(10)))), Function(Set((E(30), V(20)))), Function(Set((V(10), 123), (V(20), 456))), Function(Set((E(30), "bar"))))
    )

    val g = Morphism(Function(Set((V(10), V(1)), (V(20), V(2)))), Function(Set((E(30), E(1)))))

    val expectedResult = Graph(
      Set(V(10), V(6), V(12), V(8), V(21), V(41)),
      Set(E(61), E(10), E(4), E(6), E(12), E(8)),
      Function(Set((E(6), V(8)), (E(61), V(21)), (E(4), V(6)), (E(8), V(8)), (E(10), V(10)), (E(12), V(10)))),
      Function(Set((E(8), V(6)), (E(10), V(12)), (E(6), V(8)), (E(12), V(12)), (E(61), V(41)), (E(4), V(8)))),
      Function(Set((V(6), 42), (V(12), 42), (V(41), 456), (V(21), 123), (V(8), 40), (V(10), 42))),
      Function(Set((E(10), "foo"), (E(12), "foo"), (E(4), "foo"), (E(8), "foo"), (E(6), "bar"), (E(61), "bar")))
    )

    val expectedTrack = Morphism(
      Function(Set((V(5), V(10)), (V(3), V(6)), (V(4), V(8)), (V(6), V(12)))),
      Function(Set((E(5), E(10)), (E(2), E(4)), (E(3), E(6)), (E(4), E(8)), (E(6), E(12))))
    )

    assert(MatchApplier.build(G)(r)(g) == expectedResult)
    assert(MatchApplier.track(expectedResult) == expectedTrack)
  }
}
