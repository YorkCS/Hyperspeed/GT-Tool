package test.core

import lib.core.common.{E, Function, Graph, Rule, V}
import lib.core.rewriting.GTSystem
import org.scalatest.flatspec._
import org.scalatest.matchers.must.Matchers

class GTSystemSpec extends AnyFlatSpec with Matchers {

  it should "apply once 1" in {
    val G = Graph(Set(V(1), V(2), V(3)), Set.empty, Function.empty, Function.empty, Function(Set((V(1), 42), (V(2), 100), (V(3), 42))), Function.empty[E, Int])

    val r = Rule(
      "r",
      Graph(Set(V(1)), Set.empty, Function.empty, Function.empty, Function(Set((V(1), 42))), Function.empty[E, Int]),
      Graph.empty,
      Graph.empty
    )

    val expected = List(
      Graph(Set(V(4), V(6)), Set.empty[E], Function.empty[E, V], Function.empty[E, V], Function(Set((V(4), 100), (V(6), 42))), Function.empty[E, Int])
    )

    assert(GTSystem.applyOnce(G)(r).toList == expected)
  }

  it should "apply once 2" in {
    val G = Graph(Set(V(1), V(11), V(21)), Set.empty, Function.empty, Function.empty, Function(Set((V(1), 42), (V(11), 100), (V(21), 42))), Function.empty[E, String])

    val r = Rule(
      "r",
      Graph(Set(V(2)), Set.empty, Function.empty, Function.empty, Function(Set((V(2), 42))), Function.empty[E, String]),
      Graph(Set(V(2)), Set.empty, Function.empty, Function.empty, Function.empty[V, Int], Function.empty[E, String]),
      Graph(Set(V(2), V(5)), Set(E(3)), Function(Set((E(3), V(2)))), Function(Set((E(3), V(5)))), Function(Set((V(2), 12345), (V(5), 123456))), Function(Set((E(3), "bar"))))
    )

    val expected = List(
      Graph(Set(V(2), V(22), V(42), V(11)),Set(E(7)),Function(Set((E(7),V(2)))),Function(Set((E(7),V(11)))),Function(Set((V(2),12345), (V(22),100), (V(42),42), (V(11),123456))),Function(Set((E(7),"bar"))))
    )

    assert(GTSystem.applyOnce(G)(r).toList == expected)
  }

  it should "apply once 3" in {
    val G = Graph(Set(V(1), V(2)), Set.empty, Function.empty, Function.empty, Function(Set((V(1), 42), (V(2), 42))), Function.empty[E, Int])

    val r = Rule(
      "r",
      Graph(Set(V(7)), Set.empty, Function.empty, Function.empty, Function(Set((V(7), 42))), Function.empty[E, Int]),
      Graph(Set(V(7)), Set.empty, Function.empty, Function.empty, Function(Set((V(7), 42))), Function.empty[E, Int]),
      Graph(Set(V(7)), Set.empty, Function.empty, Function.empty, Function(Set((V(7), 42))), Function.empty[E, Int])
    )

    val expected = List(
      Graph(Set(V(2), V(4)), Set.empty, Function.empty, Function.empty, Function(Set((V(2), 42), (V(4), 42))), Function.empty[E, Int])
    )

    assert(GTSystem.applyOnce(G)(r).toList == expected)
  }

  it should "find one normal form" in {
    val G = Graph(Set(V(1), V(2), V(3)), Set.empty, Function.empty, Function.empty, Function(Set((V(1), 42), (V(2), 100), (V(3), 42))), Function.empty[E, Int])

    val r = Rule(
      "r",
      Graph(Set(V(1)), Set.empty, Function.empty, Function.empty, Function(Set((V(1), 42))), Function.empty[E, Int]),
      Graph.empty,
      Graph.empty
    )

    val expected = List(
      Graph(Set(V(8)),Set.empty[E], Function.empty[E, V], Function.empty[E, V], Function(Set((V(8),100))), Function.empty[E, Int])
    )

    assert(GTSystem.applyUntilNormal(G)(Set(r)).toList == expected)
  }

  it should "find normal forms" in {
    val G = Graph(Set(V(1), V(11), V(21)), Set.empty, Function.empty, Function.empty, Function(Set((V(1), 42), (V(11), 100), (V(21), 42))), Function.empty[E, String])

    val r = Rule(
      "r",
      Graph(Set(V(2)), Set.empty, Function.empty, Function.empty, Function(Set((V(2), 42))), Function.empty[E, String]),
      Graph(Set(V(2)), Set.empty, Function.empty, Function.empty, Function.empty[V, Int], Function.empty[E, String]),
      Graph(Set(V(2), V(5)), Set(E(3)), Function(Set((E(3), V(2)))), Function(Set((E(3), V(5)))), Function(Set((V(2), 12345), (V(5), 123456))), Function(Set((E(3), "bar"))))
    )

    val expected = List(
      Graph(Set(V(4), V(11), V(84), V(44), V(22)),Set(E(14), E(7)),Function(Set((E(14),V(4)), (E(7),V(84)))),Function(Set((E(14),V(22)), (E(7),V(11)))),Function(Set((V(4),12345), (V(44),100), (V(22),123456), (V(84),12345), (V(11),123456))),Function(Set((E(14),"bar"), (E(7),"bar"))))
    )

    assert(GTSystem.applyUntilNormal(G)(Set(r)).toList == expected)
  }

  it should "execute program" in {
    val G = Graph(Set(V(1), V(11), V(21)), Set.empty, Function.empty, Function.empty, Function(Set((V(1), 42), (V(11), 100), (V(21), 42))), Function.empty[E, String])

    val r0 = Rule(
      "r0",
      Graph(Set(V(2)), Set.empty, Function.empty, Function.empty, Function(Set((V(2), 42))), Function.empty[E, String]),
      Graph(Set(V(2)), Set.empty, Function.empty, Function.empty, Function.empty[V, Int], Function.empty[E, String]),
      Graph(Set(V(2), V(5)), Set(E(3)), Function(Set((E(3), V(2)))), Function(Set((E(3), V(5)))), Function(Set((V(2), 12345), (V(5), 123456))), Function(Set((E(3), "bar"))))
    )

    val r1 = Rule(
      "r1",
      Graph(Set(V(1)), Set.empty, Function.empty, Function.empty, Function(Set((V(1), 42))), Function.empty[E, String]),
      Graph(Set(V(1)), Set.empty, Function.empty, Function.empty, Function.empty[V, Int], Function.empty[E, String]),
      Graph(Set.empty[V], Set.empty[E], Function.empty, Function.empty, Function.empty[V, Int], Function.empty[E, String])
    )

    val r2 = Rule(
      "r2",
      Graph(Set(V(1), V(2)), Set(E(1)), Set((E(1), V(1))), Set((E(1), V(2))), Function(Set((V(1), 12345), (V(2), 123456))), Function(Set((E(1), "bar")))),
      Graph(Set(V(1), V(2)), Set.empty, Function.empty, Function.empty, Function.empty[V, Int], Function.empty[E, String]),
      Graph(Set(V(1), V(2)), Set(E(1)), Set((E(1), V(1))), Set((E(1), V(2))), Function(Set((V(1), 246), (V(2), 357))), Function(Set((E(1), "baz"))))
    )

    val expected = List(
      Graph(Set(V(88), V(176), V(44), V(336), V(16)),Set(E(6), E(3)),Function(Set((E(6),V(16)), (E(3),V(336)))),Function(Set((E(6),V(88)), (E(3),V(44)))),Function(Set((V(176),100), (V(44),357), (V(16),246), (V(336),246), (V(88),357))),Function(Set((E(6),"baz"), (E(3),"baz"))))
    )

    assert(GTSystem.executeProgram(G)(List(Set(r0), Set(r1, r2))) == expected)
  }
}
