package test.core

import lib.core.common.{E, Function, Graph, V}
import lib.core.util.IsomorphismTester
import org.scalatest.flatspec._
import org.scalatest.matchers.must.Matchers

class IsomorphismTesterSpec extends AnyFlatSpec with Matchers {

  it should "work in the trivial case" in {
    assert(IsomorphismTester.isIsomorphic(Graph.empty)(Graph.empty))
  }

  it should "basic non-example" in {
    assert(!IsomorphismTester.isIsomorphic(Graph.empty[Int, Int])(Graph(Set(V(1)), Set.empty[E], Function.empty[E, V], Function.empty[E, V], Function[V, Int](Set((V(1), 1))), Function.empty[E, Int])))
  }

  it should "label non-example 1" in {
    assert(
      !IsomorphismTester.isIsomorphic
      (Graph(Set(V(1)), Set.empty[E], Function.empty[E, V], Function.empty[E, V], Function[V, Int](Set((V(1), 2))), Function.empty[E, Int]))
      (Graph(Set(V(1)), Set.empty[E], Function.empty[E, V], Function.empty[E, V], Function[V, Int](Set((V(1), 1))), Function.empty[E, Int]))
    )
  }

  it should "label non-example 2" in {
    assert(
      !IsomorphismTester.isIsomorphic
      (Graph(Set(V(1)), Set.empty[E], Function.empty[E, V], Function.empty[E, V], Function.empty[V, Int], Function.empty[E, Int]))
      (Graph(Set(V(1)), Set.empty[E], Function.empty[E, V], Function.empty[E, V], Function[V, Int](Set((V(1), 1))), Function.empty[E, Int]))
    )
  }

  it should "positive example" in {
    assert(
      IsomorphismTester.isIsomorphic
      (Graph(Set(V(1), V(2)), Set(E(3)), Function(Set((E(3), V(1)))), Function(Set((E(3), V(1)))), Function(Set((V(1), 1), (V(2), 2))), Function(Set((E(3), 3)))))
      (Graph(Set(V(3), V(4)), Set(E(5)), Function(Set((E(5), V(4)))), Function(Set((E(5), V(4)))), Function(Set((V(3), 2), (V(4), 1))), Function(Set((E(5), 3)))))
    )
  }
}
