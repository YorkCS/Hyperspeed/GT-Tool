package test.core

import lib.core.analysis
import lib.core.analysis.{CriticalPair, CriticalPairGenerator, JoinabilityTester}
import lib.core.common.{Derivation, E, Function, Graph, Morphism, Rule, V}
import org.scalatest.flatspec._
import org.scalatest.matchers.must.Matchers

class CriticalPairGeneratorSpec extends AnyFlatSpec with Matchers {

  it should "generate no pairs 1" in {
    assert(CriticalPairGenerator.generateCriticalPairs(Set.empty[Rule[Unit, Unit]]).isEmpty)
  }

  it should "generate no pairs 2" in {
    val rules = Set(Rule(
      "r1",
      Graph.unlabelled(Set(V(1)), Set.empty, Function.empty, Function.empty),
      Graph.empty[Unit, Unit],
      Graph.unlabelled(Set(V(1)), Set.empty, Function.empty, Function.empty)
    ))

    assert(CriticalPairGenerator.generateCriticalPairs(rules).isEmpty)
  }

  it should "generate some pairs 1" in {
    val rules = Set(
      Rule(
        "r1",
        Graph.unlabelled(Set(V(1), V(3), V(2)), Set(E(1), E(2)), Set((E(1), V(1)), (E(2), V(3))), Set((E(1), V(3)), (E(2), V(2)))),
        Graph.unlabelled(Set(V(1), V(2)), Set.empty, Function.empty, Function.empty),
        Graph.unlabelled(Set(V(1), V(2)), Set(E(1)), Set((E(1), V(1))), Set((E(1), V(2)))),
      ),
      Rule(
        "r2",
        Graph.unlabelled(Set(V(1), V(3), V(2)), Set(E(1), E(2)), Set((E(1), V(1)), (E(2), V(2))), Set((E(1), V(3)), (E(2), V(3)))),
        Graph.unlabelled(Set(V(1), V(2)), Set.empty, Function.empty, Function.empty),
        Graph.unlabelled(Set(V(1), V(2)), Set(E(1)), Set((E(1), V(1))), Set((E(1), V(2)))),
      )
    )

    assert(CriticalPairGenerator.generateCriticalPairs(rules).length == 7)
    assert(CriticalPairGenerator.generateCriticalPairs(rules).count(JoinabilityTester.isJoinable(rules)) === 6)
    assert(CriticalPairGenerator.generateCriticalPairs(rules).count(JoinabilityTester.isStronglyJoinable(rules)) === 5)
  }

  it should "generate some pairs 2" in {
    val rules = Set(
      Rule(
        "r1",
        Graph.unlabelled(Set(V(1), V(3), V(2)), Set(E(1), E(2)), Set((E(1), V(1)), (E(2), V(3))), Set((E(1), V(3)), (E(2), V(2)))),
        Graph.unlabelled(Set(V(1), V(2)), Set.empty, Function.empty, Function.empty),
        Graph.unlabelled(Set(V(1), V(2)), Set(E(1)), Set((E(1), V(1))), Set((E(1), V(2)))),
      ),
      Rule(
        "r2",
        Graph.unlabelled(Set(V(1), V(2)), Set(E(1)), Set((E(1), V(1))), Set((E(1), V(2)))),
        Graph.unlabelled(Set(V(1)), Set.empty, Function.empty, Function.empty),
        Graph.unlabelled(Set(V(1)), Set.empty, Function.empty, Function.empty),
      ),
      Rule(
        "r3",
        Graph.unlabelled(Set(V(1), V(2)), Set(E(1)), Set((E(1), V(1))), Set((E(1), V(2)))),
        Graph.unlabelled(Set(V(2)), Set.empty, Function.empty, Function.empty),
        Graph.unlabelled(Set(V(2)), Set.empty, Function.empty, Function.empty),
      ),
      Rule(
        "r4",
        Graph.unlabelled(Set(V(1), V(2)), Set(E(1), E(2)), Set((E(1), V(1)), (E(2), V(2))), Set((E(1), V(2)), (E(2), V(1)))),
        Graph.unlabelled(Set(V(1)), Set.empty, Function.empty, Function.empty),
        Graph.unlabelled(Set(V(1)), Set.empty, Function.empty, Function.empty),
      )
    )

    assert(CriticalPairGenerator.generateCriticalPairs(rules).length == 6)
    assert(CriticalPairGenerator.generateCriticalPairs(rules).count(JoinabilityTester.isJoinable(rules)) === 6)
    assert(CriticalPairGenerator.generateCriticalPairs(rules).count(JoinabilityTester.isStronglyJoinable(rules)) === 6)
  }

  it should "generate some pairs 3" in {
    val rules = Set(
      Rule(
        "r1",
        Graph.unlabelled(Set(V(1), V(2)), Set(E(1), E(2)), Set((E(1), V(1)), (E(2), V(2))), Set((E(1), V(1)), (E(2), V(2)))),
        Graph.unlabelled(Set(V(1), V(2)), Set(E(1)), Set((E(1), V(1))), Set((E(1), V(1)))),
        Graph.unlabelled(Set(V(1), V(2)), Set(E(1)), Set((E(1), V(1))), Set((E(1), V(1)))),
      ),
      Rule(
        "r2",
        Graph.unlabelled(Set(V(1)), Set(E(1), E(2)), Set((E(1), V(1)), (E(2), V(1))), Set((E(1), V(1)), (E(2), V(1)))),
        Graph.unlabelled(Set(V(1)), Set(E(1)), Set((E(1), V(1))), Set((E(1), V(1)))),
        Graph.unlabelled(Set(V(1)), Set(E(1)), Set((E(1), V(1))), Set((E(1), V(1)))),
      ),
      Rule(
        "r3",
        Graph.unlabelled(Set(V(1), V(2)), Set(E(1)), Set((E(1), V(1))), Set((E(1), V(2)))),
        Graph.unlabelled(Set(V(1), V(2)), Set.empty, Function.empty, Function.empty),
        Graph.unlabelled(Set(V(1), V(2)), Set.empty, Function.empty, Function.empty),
      ),
    )

    assert(CriticalPairGenerator.generateCriticalPairs(rules).length == 7)
    assert(CriticalPairGenerator.generateCriticalPairs(rules).count(JoinabilityTester.isJoinable(rules)) === 7)
    assert(CriticalPairGenerator.generateCriticalPairs(rules).count(JoinabilityTester.isStronglyJoinable(rules)) === 6)
  }

  it should "generate some pairs 4" in {
    val rules = Set(
      Rule(
        "r1",
        Graph.unlabelled(Set(V(1)), Set.empty, Function.empty, Function.empty),
        Graph.unlabelled(Set.empty, Set.empty, Function.empty, Function.empty),
        Graph.unlabelled(Set.empty, Set.empty, Function.empty, Function.empty),
      ),
      Rule(
        "r2",
        Graph.unlabelled(Set(V(1)), Set.empty, Function.empty, Function.empty),
        Graph.unlabelled(Set(V(1)), Set.empty, Function.empty, Function.empty),
        Graph.unlabelled(Set(V(1)), Set(E(1)), Set((E(1), V(1))), Set((E(1), V(1)))),
      ),
      Rule(
        "r3",
        Graph.unlabelled(Set(V(1)), Set(E(1)), Set((E(1), V(1))), Set((E(1), V(1)))),
        Graph.unlabelled(Set.empty, Set.empty, Function.empty, Function.empty),
        Graph.unlabelled(Set(V(1)), Set.empty, Function.empty, Function.empty),
      )
    )

    val expected = List(
      CriticalPair(
        Graph.unlabelled(Set(V(2)),Set.empty,Function.empty,Function.empty),
        Derivation(Rule("r1",Graph(Set(V(1)),Set.empty,Function.empty,Function.empty,Function(Set((V(1),()))),Function.empty[E, Unit]),Graph(Set.empty,Set.empty,Function.empty,Function.empty,Function.empty[V, Unit],Function.empty[E, Unit]),Graph(Set.empty,Set.empty,Function.empty,Function.empty,Function.empty[V, Unit],Function.empty[E, Unit])),Morphism(Function(Set((V(1),V(2)))),Function.empty)),
        Derivation(Rule("r2",Graph(Set(V(1)),Set.empty,Function.empty,Function.empty,Function(Set((V(1),()))),Function.empty[E, Unit]),Graph(Set(V(1)),Set.empty,Function.empty,Function.empty,Function(Set((V(1),()))),Function.empty[E, Unit]),Graph(Set(V(1)),Set(E(1)),Function(Set((E(1),V(1)))),Function(Set((E(1),V(1)))),Function(Set((V(1),()))),Function(Set((E(1),()))))),Morphism(Function(Set((V(1),V(2)))),Function.empty))
      ),
      analysis.CriticalPair(
        Graph.unlabelled(Set(V(2)),Set(E(3)),Function(Set((E(3),V(2)))),Function(Set((E(3),V(2))))),
        Derivation(Rule("r2",Graph(Set(V(1)),Set.empty,Function.empty,Function.empty,Function(Set((V(1),()))),Function.empty[E, Unit]),Graph(Set(V(1)),Set.empty,Function.empty,Function.empty,Function(Set((V(1),()))),Function.empty[E, Unit]),Graph(Set(V(1)),Set(E(1)),Function(Set((E(1),V(1)))),Function(Set((E(1),V(1)))),Function(Set((V(1),()))),Function(Set((E(1),()))))),Morphism(Function(Set((V(1),V(2)))),Function.empty)),
        Derivation(Rule("r3",Graph(Set(V(1)),Set(E(1)),Function(Set((E(1),V(1)))),Function(Set((E(1),V(1)))),Function(Set((V(1),()))),Function(Set((E(1),())))),Graph(Set.empty,Set.empty,Function.empty,Function.empty,Function.empty[V, Unit],Function.empty[E, Unit]),Graph(Set(V(1)),Set.empty,Function.empty,Function.empty,Function(Set((V(1),()))),Function.empty[E, Unit])),Morphism(Function(Set((V(1),V(2)))),Function(Set((E(1),E(3)))))
        )
      )
    )

    assert(CriticalPairGenerator.generateCriticalPairs(rules) == expected)
  }

  it should "generate some pairs 5" in {
    val rules: Set[Rule[Unit, Unit]] = Set(
      Rule(
        "r1",
        Graph.discrete(Set((V(1), ()))),
        Graph.discrete(Set((V(1), ()))),
        Graph.discrete(Set((V(1), ()), (V(2), ())))
      ),
      Rule(
        "r2",
        Graph.discrete(Set((V(1), ()))),
        Graph.empty,
        Graph.discrete(Set((V(1), ()), (V(2), ())))
      ),
      Rule(
        "r3",
        Graph(Set(V(1)), Set(E(1)), Set((E(1), V(1))), Set((E(1), V(1))), Set((V(1), ())), Set((E(1), ()))),
        Graph.empty,
        Graph.empty
      ),
    )

    val expected: List[CriticalPair[Unit, Unit]] = List(
      CriticalPair(Graph(Set(V(2)),Set(),Function(Set()),Function(Set()),Function(Set((V(2),()))),Function(Set())),Derivation(Rule("r1",Graph(Set(V(1)),Set(),Function(Set()),Function(Set()),Function(Set((V(1),()))),Function(Set())),Graph(Set(V(1)),Set(),Function(Set()),Function(Set()),Function(Set((V(1),()))),Function(Set())),Graph(Set(V(1), V(2)),Set(),Function(Set()),Function(Set()),Function(Set((V(1),()), (V(2),()))),Function(Set()))),Morphism(Function(Set((V(1),V(2)))),Function(Set()))),Derivation(Rule("r2",Graph(Set(V(1)),Set(),Function(Set()),Function(Set()),Function(Set((V(1),()))),Function(Set())),Graph(Set(),Set(),Function(Set()),Function(Set()),Function(Set()),Function(Set())),Graph(Set(V(1), V(2)),Set(),Function(Set()),Function(Set()),Function(Set((V(1),()), (V(2),()))),Function(Set()))),Morphism(Function(Set((V(1),V(2)))),Function(Set())))),
      CriticalPair(Graph(Set(V(2)),Set(E(3)),Function(Set((E(3),V(2)))),Function(Set((E(3),V(2)))),Function(Set((V(2),()))),Function(Set((E(3),())))),Derivation(Rule("r1",Graph(Set(V(1)),Set(),Function(Set()),Function(Set()),Function(Set((V(1),()))),Function(Set())),Graph(Set(V(1)),Set(),Function(Set()),Function(Set()),Function(Set((V(1),()))),Function(Set())),Graph(Set(V(1), V(2)),Set(),Function(Set()),Function(Set()),Function(Set((V(1),()), (V(2),()))),Function(Set()))),Morphism(Function(Set((V(1),V(2)))),Function(Set()))),Derivation(Rule("r3",Graph(Set(V(1)),Set(E(1)),Function(Set((E(1),V(1)))),Function(Set((E(1),V(1)))),Function(Set((V(1),()))),Function(Set((E(1),())))),Graph(Set(),Set(),Function(Set()),Function(Set()),Function(Set()),Function(Set())),Graph(Set(),Set(),Function(Set()),Function(Set()),Function(Set()),Function(Set()))),Morphism(Function(Set((V(1),V(2)))),Function(Set((E(1),E(3)))))))
    )

    assert(CriticalPairGenerator.generateCriticalPairs(rules) == expected)
  }

  it should "generate some pairs 6" in {
    val rules: Set[Rule[Unit, Unit]] = Set(
      Rule(
        "r1",
        Graph(Set(V(1), V(2), V(3)), Set(E(1), E(2)), Set((E(1), V(1)), (E(2), V(3))), Set((E(1), V(3)), (E(2), V(2))), Set((V(1), ()), (V(2), ()), (V(3), ())), Set((E(1), ()), (E(2), ()))),
        Graph(Set(V(1), V(2)), Set.empty[E], Function.empty[E, V], Function.empty[E, V], Set((V(1), ()), (V(2), ())), Function.empty[E, Unit]),
        Graph(Set(V(1), V(2)), Set(E(1)), Set((E(1), V(1))), Set((E(1), V(2))), Set((V(1), ()), (V(2), ())), Set((E(1), ()))),
      ),
    )

    val expected: List[CriticalPair[Unit, Unit]] = List(
      CriticalPair(Graph(Set(V(2), V(4), V(6)),Set(E(2), E(4), E(3)),Function(Set((E(2),V(2)), (E(4),V(6)), (E(3),V(4)))),Function(Set((E(2),V(6)), (E(4),V(4)), (E(3),V(2)))),Function(Set((V(2),()), (V(4),()), (V(6),()))),Function(Set((E(2),()), (E(4),()), (E(3),())))),Derivation(Rule("r1",Graph(Set(V(1), V(2), V(3)),Set(E(1), E(2)),Function(Set((E(1),V(1)), (E(2),V(3)))),Function(Set((E(1),V(3)), (E(2),V(2)))),Function(Set((V(1),()), (V(2),()), (V(3),()))),Function(Set((E(1),()), (E(2),())))),Graph(Set(V(1), V(2)),Set(),Function(Set()),Function(Set()),Function(Set((V(1),()), (V(2),()))),Function(Set())),Graph(Set(V(1), V(2)),Set(E(1)),Function(Set((E(1),V(1)))),Function(Set((E(1),V(2)))),Function(Set((V(1),()), (V(2),()))),Function(Set((E(1),()))))),Morphism(Function(Set((V(1),V(2)), (V(2),V(4)), (V(3),V(6)))),Function(Set((E(1),E(2)), (E(2),E(4)))))),Derivation(Rule("r1",Graph(Set(V(1), V(2), V(3)),Set(E(1), E(2)),Function(Set((E(1),V(1)), (E(2),V(3)))),Function(Set((E(1),V(3)), (E(2),V(2)))),Function(Set((V(1),()), (V(2),()), (V(3),()))),Function(Set((E(1),()), (E(2),())))),Graph(Set(V(1), V(2)),Set(),Function(Set()),Function(Set()),Function(Set((V(1),()), (V(2),()))),Function(Set())),Graph(Set(V(1), V(2)),Set(E(1)),Function(Set((E(1),V(1)))),Function(Set((E(1),V(2)))),Function(Set((V(1),()), (V(2),()))),Function(Set((E(1),()))))),Morphism(Function(Set((V(3),V(2)), (V(1),V(4)), (V(2),V(6)))),Function(Set((E(2),E(2)), (E(1),E(3))))))),
      CriticalPair(Graph(Set(V(2), V(4), V(6), V(5)),Set(E(2), E(4), E(5)),Function(Set((E(2),V(2)), (E(4),V(6)), (E(5),V(4)))),Function(Set((E(2),V(6)), (E(4),V(4)), (E(5),V(5)))),Function(Set((V(2),()), (V(4),()), (V(6),()), (V(5),()))),Function(Set((E(2),()), (E(4),()), (E(5),())))),Derivation(Rule("r1",Graph(Set(V(1), V(2), V(3)),Set(E(1), E(2)),Function(Set((E(1),V(1)), (E(2),V(3)))),Function(Set((E(1),V(3)), (E(2),V(2)))),Function(Set((V(1),()), (V(2),()), (V(3),()))),Function(Set((E(1),()), (E(2),())))),Graph(Set(V(1), V(2)),Set(),Function(Set()),Function(Set()),Function(Set((V(1),()), (V(2),()))),Function(Set())),Graph(Set(V(1), V(2)),Set(E(1)),Function(Set((E(1),V(1)))),Function(Set((E(1),V(2)))),Function(Set((V(1),()), (V(2),()))),Function(Set((E(1),()))))),Morphism(Function(Set((V(1),V(2)), (V(2),V(4)), (V(3),V(6)))),Function(Set((E(1),E(2)), (E(2),E(4)))))),Derivation(Rule("r1",Graph(Set(V(1), V(2), V(3)),Set(E(1), E(2)),Function(Set((E(1),V(1)), (E(2),V(3)))),Function(Set((E(1),V(3)), (E(2),V(2)))),Function(Set((V(1),()), (V(2),()), (V(3),()))),Function(Set((E(1),()), (E(2),())))),Graph(Set(V(1), V(2)),Set(),Function(Set()),Function(Set()),Function(Set((V(1),()), (V(2),()))),Function(Set())),Graph(Set(V(1), V(2)),Set(E(1)),Function(Set((E(1),V(1)))),Function(Set((E(1),V(2)))),Function(Set((V(1),()), (V(2),()))),Function(Set((E(1),()))))),Morphism(Function(Set((V(3),V(4)), (V(1),V(6)), (V(2),V(5)))),Function(Set((E(1),E(4)), (E(2),E(5)))))))
    )

    assert(CriticalPairGenerator.generateCriticalPairs(rules) == expected)
  }
}
