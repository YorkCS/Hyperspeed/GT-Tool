package examples

import lib.core.analysis._
import lib.core.common._

object TreeReduction {

  val rules: Set[Rule[Unit, Unit]] = Set(
    Rule(
      "prune",
      Graph(Set(V(1), V(2)), Set(E(1)), Set((E(1), V(1))), Set((E(1), V(2))), Set((V(1), ()), (V(2), ())), Set((E(1), ()))),
      Graph(Set(V(1)), Set.empty[E], Function.empty[E, V], Function.empty[E, V], Set((V(1), ())), Function.empty[E, Unit]),
      Graph(Set(V(1)), Set.empty[E], Function.empty[E, V], Function.empty[E, V], Set((V(1), ())), Function.empty[E, Unit]),
    ),
  )

  def main(args: Array[String]): Unit = {
    println("RULES:")
    println()

    rules.foreach(r => {
      println(r.name ++ ":")
      println(Printer.printGraph(r.l))
      println(Printer.printGraph(r.k))
      println(Printer.printGraph(r.r))
      println()
    })

    val pairs = CriticalPairGenerator.generateCriticalPairs(rules)
    print("CRITICAL PAIRS: ")
    println(pairs.length)

    val essential = pairs.filter(EssentialTester.isEssential)
    print("ESSENTIAL PAIRS: ")
    println(essential.length)

    val joinable = pairs.filter(JoinabilityTester.isJoinable(rules))
    print("JOINABLE PAIRS: ")
    println(joinable.length)

    val strongly = pairs.filter(JoinabilityTester.isStronglyJoinable(rules))
    print("STRONGLY JOINABLE: ")
    println(strongly.length)

    println()

    pairs.foreach(p => {
      println(CriticalPairPrinter.printCriticalPair(p))
      println(CriticalPairPrinter.printEssentialStatus(p))
      println(CriticalPairPrinter.printJoinabilityStatus(rules)(p))
      println(CriticalPairPrinter.printStrongJoinabilityStatus(rules)(p))
      println(CriticalPairPrinter.printConflictReason(p))
      println()
    })
  }
}
