package examples.paper

import lib.core.analysis._
import lib.core.common._

object SubcommutativeExample {

  val rules: Set[Rule[Unit, Unit]] = Set(
    Rule(
      "r1",
      Graph.discrete(Set((V(1), ()))),
      Graph.discrete(Set((V(1), ()))),
      Graph.discrete(Set((V(1), ()), (V(2), ())))
    ),
    Rule(
      "r2",
      Graph.discrete(Set((V(1), ()))),
      Graph.empty,
      Graph.discrete(Set((V(1), ()), (V(2), ())))
    ),
    Rule(
      "r3",
      Graph(Set(V(1)), Set(E(1)), Set((E(1), V(1))), Set((E(1), V(1))), Set((V(1), ())), Set((E(1), ()))),
      Graph.empty,
      Graph.empty
    ),
  )

  def main(args: Array[String]): Unit = {
    println("RULES:")
    println()

    rules.foreach(r => {
      println(r.name ++ ":")
      println(Printer.printGraph(r.l))
      println(Printer.printGraph(r.k))
      println(Printer.printGraph(r.r))
      println()
    })

    val pairs = CriticalPairGenerator.generateCriticalPairs(rules)
    print("CRITICAL PAIRS: ")
    println(pairs.length)

    val essential = pairs.filter(EssentialTester.isEssential)
    print("ESSENTIAL PAIRS: ")
    println(essential.length)

    val subcommutative = pairs.filter(JoinabilityTester.isSubcommutative(rules))
    print("SUBCOMMUTATIVE PAIRS: ")
    println(subcommutative.length)

    val stronglySubcommutative = pairs.filter(JoinabilityTester.isStronglySubcommutative(rules))
    print("STRONGLY SUBCOMMUTATIVE: ")
    println(stronglySubcommutative.length)

    println()

    pairs.foreach(p => {
      println(CriticalPairPrinter.printCriticalPair(p))
      println(CriticalPairPrinter.printEssentialStatus(p))
      println(CriticalPairPrinter.printSubcommutativityStatus(rules)(p))
      println(CriticalPairPrinter.printStrongSubcommutativityStatus(rules)(p))
      println(CriticalPairPrinter.printConflictReason(p))
      println()
    })
  }
}
