package examples

import lib.core.analysis._
import lib.core.common._

object LabelledBinaryDAG {

  val rules: Set[Rule[Unit, String]] = Set(
    Rule(
      "r1",
      Graph.discrete(Set((V(1), ()))),
      Graph.empty,
      Graph.empty,
    ),
    Rule(
      "r2a",
      Graph(Set(V(1), V(2)), Set(E(1)), Set((E(1), V(1))), Set((E(1), V(2))), Set((V(1), ()), (V(2), ())), Set((E(1), "a"))),
      Graph.discrete(Set((V(2), ()))),
      Graph.discrete(Set((V(2), ()))),
    ),
    Rule(
      "r2b",
      Graph(Set(V(1), V(2)), Set(E(1)), Set((E(1), V(1))), Set((E(1), V(2))), Set((V(1), ()), (V(2), ())), Set((E(1), "b"))),
      Graph.discrete(Set((V(2), ()))),
      Graph.discrete(Set((V(2), ()))),
    ),
    Rule(
      "r3aa",
      Graph(Set(V(1), V(2)), Set(E(1), E(2)), Set((E(1), V(1)), (E(2), V(1))), Set((E(1), V(2)), (E(2), V(2))), Set((V(1), ()), (V(2), ())), Set((E(1), "a"), (E(2), "a"))),
      Graph.discrete(Set((V(2), ()))),
      Graph.discrete(Set((V(2), ()))),
    ),
    Rule(
      "r3ab",
      Graph(Set(V(1), V(2)), Set(E(1), E(2)), Set((E(1), V(1)), (E(2), V(1))), Set((E(1), V(2)), (E(2), V(2))), Set((V(1), ()), (V(2), ())), Set((E(1), "a"), (E(2), "b"))),
      Graph.discrete(Set((V(2), ()))),
      Graph.discrete(Set((V(2), ()))),
    ),
    Rule(
      "r3bb",
      Graph(Set(V(1), V(2)), Set(E(1), E(2)), Set((E(1), V(1)), (E(2), V(1))), Set((E(1), V(2)), (E(2), V(2))), Set((V(1), ()), (V(2), ())), Set((E(1), "b"), (E(2), "b"))),
      Graph.discrete(Set((V(2), ()))),
      Graph.discrete(Set((V(2), ()))),
    ),
    Rule(
      "r4aa",
      Graph(Set(V(1), V(2), V(3)), Set(E(1), E(2)), Set((E(1), V(1)), (E(2), V(1))), Set((E(1), V(2)), (E(2), V(3))), Set((V(1), ()), (V(2), ()), (V(3), ())), Set((E(1), "a"), (E(2), "a"))),
      Graph.discrete(Set((V(2), ()), (V(3), ()))),
      Graph.discrete(Set((V(2), ()), (V(3), ()))),
    ),
    Rule(
      "r4ab",
      Graph(Set(V(1), V(2), V(3)), Set(E(1), E(2)), Set((E(1), V(1)), (E(2), V(1))), Set((E(1), V(2)), (E(2), V(3))), Set((V(1), ()), (V(2), ()), (V(3), ())), Set((E(1), "a"), (E(2), "b"))),
      Graph.discrete(Set((V(2), ()), (V(3), ()))),
      Graph.discrete(Set((V(2), ()), (V(3), ()))),
    ),
    Rule(
      "r4bb",
      Graph(Set(V(1), V(2), V(3)), Set(E(1), E(2)), Set((E(1), V(1)), (E(2), V(1))), Set((E(1), V(2)), (E(2), V(3))), Set((V(1), ()), (V(2), ()), (V(3), ())), Set((E(1), "b"), (E(2), "b"))),
      Graph.discrete(Set((V(2), ()), (V(3), ()))),
      Graph.discrete(Set((V(2), ()), (V(3), ()))),
    ),
  )

  def main(args: Array[String]): Unit = {
    println("RULES:")
    println()

    rules.foreach(r => {
      println(r.name ++ ":")
      println(Printer.printGraph(r.l))
      println(Printer.printGraph(r.k))
      println(Printer.printGraph(r.r))
      println()
    })

    val pairs = CriticalPairGenerator.generateCriticalPairs(rules)
    print("CRITICAL PAIRS: ")
    println(pairs.length)

    val essential = pairs.filter(EssentialTester.isEssential)
    print("ESSENTIAL PAIRS: ")
    println(essential.length)

    val joinable = pairs.filter(JoinabilityTester.isJoinable(rules))
    print("JOINABLE PAIRS: ")
    println(joinable.length)

    val strongly = pairs.filter(JoinabilityTester.isStronglyJoinable(rules))
    print("STRONGLY JOINABLE: ")
    println(strongly.length)

    println()

    pairs.foreach(p => {
      println(CriticalPairPrinter.printCriticalPair(p))
      println(CriticalPairPrinter.printEssentialStatus(p))
      println(CriticalPairPrinter.printJoinabilityStatus(rules)(p))
      println(CriticalPairPrinter.printStrongJoinabilityStatus(rules)(p))
      println(CriticalPairPrinter.printConflictReason(p))
      println()
    })
  }
}
