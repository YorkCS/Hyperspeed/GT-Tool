package examples

import lib.core.analysis._
import lib.core.common._

object GRATExample {

  val rules: Set[Rule[Unit, Option[Boolean]]] = Set(
    Rule(
      "r1",
      Graph(Set(V(1), V(2), V(3)), Set(E(1), E(2)), Set((E(1), V(1)), (E(2), V(3))), Set((E(1), V(3)), (E(2), V(2))), Set((V(1), ()), (V(2), ()), (V(3), ())), Set((E(1), Option.empty[Boolean]), (E(2), Option.empty[Boolean]))),
      Graph.discrete(Set((V(1), ()), (V(2), ()))),
      Graph(Set(V(1), V(2)), Set(E(1), E(2)), Set((E(1), V(1)), (E(2), V(1))), Set((E(1), V(2)), (E(2), V(2))), Set((V(1), ()), (V(2), ())), Set((E(1), Option(false)), (E(2), Option(true)))),
    ),
    Rule(
      "r2",
      Graph(Set(V(1), V(2)), Set(E(1), E(2)), Set((E(1), V(1)), (E(2), V(1))), Set((E(1), V(2)), (E(2), V(2))), Set((V(1), ()), (V(2), ())), Set((E(1), Option(false)), (E(2), Option(true)))),
      Graph.discrete(Set((V(1), ()), (V(2), ()))),
      Graph(Set(V(1), V(2)), Set(E(1)), Set((E(1), V(1))), Set((E(1), V(2))), Set((V(1), ()), (V(2), ())), Set((E(1), Option.empty[Boolean]))),
    ),
  )

  def main(args: Array[String]): Unit = {
    println("RULES:")
    println()

    rules.foreach(r => {
      println(r.name ++ ":")
      println(Printer.printGraph(r.l))
      println(Printer.printGraph(r.k))
      println(Printer.printGraph(r.r))
      println()
    })

    val pairs = CriticalPairGenerator.generateCriticalPairs(rules)
    print("CRITICAL PAIRS: ")
    println(pairs.length)

    val essential = pairs.filter(EssentialTester.isEssential)
    print("ESSENTIAL PAIRS: ")
    println(essential.length)

    val joinable = pairs.filter(JoinabilityTester.isJoinable(rules))
    print("JOINABLE PAIRS: ")
    println(joinable.length)

    val strongly = pairs.filter(JoinabilityTester.isStronglyJoinable(rules))
    print("STRONGLY JOINABLE: ")
    println(strongly.length)

    println()

    pairs.foreach(p => {
      println(CriticalPairPrinter.printCriticalPair(p))
      println(CriticalPairPrinter.printEssentialStatus(p))
      println(CriticalPairPrinter.printJoinabilityStatus(rules)(p))
      println(CriticalPairPrinter.printStrongJoinabilityStatus(rules)(p))
      println(CriticalPairPrinter.printConflictReason(p))
      println()
    })
  }
}
