package examples.programs

import lib.core.common._
import lib.gp2.common._
import lib.gp2.interpreter.Evaluator

object TreeRecognition {

  val not_empty: GP2Rule = Rule(
    "not_empty",
    Graph(Set(V(1)), Set.empty[E], Function.empty[E, V], Function.empty[E, V], Function(Set((V(1), (Ident(1), Grey())))), Function.empty[E, (RL, EM)]),
    Graph(Set(V(1)), Set.empty[E], Function.empty[E, V], Function.empty[E, V], Function.empty[V, (RL, VM)], Function.empty[E, (RL, EM)]),
    Graph(Set(V(1)), Set.empty[E], Function.empty[E, V], Function.empty[E, V], Function(Set((V(1), (Ident(1), Grey())))), Function.empty[E, (RL, EM)]),
  )

  val prune: GP2Rule = Rule(
    "prune",
    Graph(Set(V(1), V(2)), Set(E(1)), Function(Set((E(1), V(1)))), Function(Set((E(1), V(2)))), Function(Set((V(1), (Ident(1), Grey())), (V(2), (Ident(2), Grey())))), Function(Set((E(1), (Ident(3), Undashed()))))),
    Graph(Set(V(1)), Set.empty[E], Function.empty[E, V], Function.empty[E, V], Function.empty[V, (RL, VM)], Function.empty[E, (RL, EM)]),
    Graph(Set(V(1)), Set.empty[E], Function.empty[E, V], Function.empty[E, V], Function(Set((V(1), (Ident(1), Grey())))), Function.empty[E, (RL, EM)]),
  )

  val two_nodes: GP2Rule = Rule(
    "two_nodes",
    Graph(Set(V(1), V(2)), Set.empty[E], Function.empty[E, V], Function.empty[E, V], Function(Set((V(1), (Ident(1), Grey())), (V(2), (Ident(2), Grey())))), Function.empty[E, (RL, EM)]),
    Graph(Set(V(1), V(2)), Set.empty[E], Function.empty[E, V], Function.empty[E, V], Function.empty[V, (RL, VM)], Function.empty[E, (RL, EM)]),
    Graph(Set(V(1), V(2)), Set.empty[E], Function.empty[E, V], Function.empty[E, V], Function(Set((V(1), (Ident(1), Grey())), (V(2), (Ident(2), Grey())))), Function.empty[E, (RL, EM)]),
  )

  val has_loop: GP2Rule = Rule(
    "has_loop",
    Graph(Set(V(1)), Set(E(1)), Function(Set((E(1), V(1)))), Function(Set((E(1), V(1)))), Function(Set((V(1), (Ident(1), Grey())))), Function(Set((E(1), (Ident(2), Undashed()))))),
    Graph(Set(V(1)), Set.empty[E], Function.empty[E, V], Function.empty[E, V], Function.empty[V, (RL, VM)], Function.empty[E, (RL, EM)]),
    Graph(Set(V(1)), Set(E(1)), Function(Set((E(1), V(1)))), Function(Set((E(1), V(1)))), Function(Set((V(1), (Ident(1), Grey())))), Function(Set((E(1), (Ident(2), Undashed()))))),
  )

  val program = Sequence(RuleSet(Set(not_empty)), Sequence(RuleSet(Set(prune)), IfThen(RuleSet(Set(two_nodes, has_loop)), Fail())))

  def main(args: Array[String]): Unit = {
    val input: HostGraph = Graph(Set(V(1), V(2)), Set(E(1)), Function(Set((E(1), V(1)))), Function(Set((E(1), V(2)))), Function(Set((V(1), (List(1, 2, 3), Grey())), (V(2), (List(42), Grey())))), Function(Set((E(1), (List(123), Undashed())))))

    println(Evaluator.eval(program)(input).map(Printer.printGraph))
  }
}
