package lib.core.matching

import lib.core.common._
import lib.core.limits.{POCFactory, POFactory}

object MatchApplier {

  def build[LV, LE](host: Graph[LV, LE])(rule: Rule[LV, LE])(g: Morphism): Graph[LV, LE] = {
    val pair = POCFactory.build(Pair(rule.k, Morphism.id(rule.k), rule.l, g, host))
    POFactory.buildGraph(
      Span(rule.k, (pair.g1, pair.G2), (Morphism.id(rule.k), rule.r))
    )
  }

  def track[LV, LE](result: Graph[LV, LE]): Morphism =
    Morphism(
      Function(result.vs.filter(v => v.value % 2 == 0).map(v => (V(v.value / 2), v))),
      Function(result.es.filter(e => e.value % 2 == 0).map(e => (E(e.value / 2), e)))
    )
}
