package lib.core.matching

import lib.core.common.{Function, Graph, Morphism, Rule}
import lib.core.util.{IsomorphismTester, SubgraphFinder}

object MatchFinder {

  def findMatches[LV, LE](host: Graph[LV, LE])(rule: Rule[LV, LE]): Iterable[Morphism] =
    SubgraphFinder.find(host)(rule.l).flatMap(computeMatches(host)(rule))

  def findPreMatches[LV, LE](host: Graph[LV, LE])(rule: Rule[LV, LE]): Iterable[Morphism] =
    SubgraphFinder.find(host)(rule.l).flatMap(computePreMatches(host)(rule))

  def computeMatches[LV, LE](host: Graph[LV, LE])(rule: Rule[LV, LE])(image: Graph[LV, LE]): Iterable[Morphism] =
    IsomorphismTester.computeIsomorphisms(rule.l)(image)
      .filter(satisfiesDanglingCondition(rule)(host))

  def computePreMatches[LV, LE](host: Graph[LV, LE])(rule: Rule[LV, LE])(image: Graph[LV, LE]): Iterable[Morphism] =
    IsomorphismTester.computePreIsomorphisms(rule.l)(image)
      .filter(satisfiesDanglingCondition(rule)(host))

  def satisfiesDanglingCondition[LV, LE](rule: Rule[LV, LE])(host: Graph[LV, LE])(g: Morphism): Boolean = {
    val vs = Function.image(g.v)(rule.l.vs diff rule.k.vs)
    val es = host.es diff Function.image(g.e)(rule.l.es)
    ((Function.image(host.s)(es) union Function.image(host.t)(es)) intersect vs).isEmpty
  }
}
