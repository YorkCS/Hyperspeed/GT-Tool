package lib.core.rewriting

import lib.core.common.{Graph, Rule}
import lib.core.matching.{MatchApplier, MatchFinder}
import lib.core.util.IsomorphismTester

import scala.collection.mutable

object GTSystem {

  def invert[LV, LE](rules: Set[Rule[LV, LE]]): Set[Rule[LV, LE]]
    = rules.map{ case Rule(name, l, k, r) => Rule(name ++ "^{-1}", r, k, l) }

  def applyOnceRaw[LV, LE](host: Graph[LV, LE])(rule: Rule[LV, LE]): Iterable[Graph[LV, LE]] =
    MatchFinder.findMatches(host)(rule).map(MatchApplier.build(host)(rule))

  def applyOnce[LV, LE](host: Graph[LV, LE])(rule: Rule[LV, LE]): Iterable[Graph[LV, LE]] =
    IsomorphismTester.upToIsomorphism(applyOnceRaw(host)(rule))

  // THIS WILL TERMINATE IF THE ONLY INFINITE DERIVATIONS ARE CYCLES
  def applyUntilNormal[LV, LE](host: Graph[LV, LE])(rules: Set[Rule[LV, LE]]): Iterable[Graph[LV, LE]] = {
    val seen = mutable.Set[Graph[LV, LE]](host)
    val results = mutable.ListBuffer[Graph[LV, LE]]()
    val worklist = mutable.Queue[Graph[LV, LE]](host)

    while (worklist.nonEmpty) {
      val t0 = worklist.dequeue()
      val t1 = rules.flatMap(applyOnce(t0))
      if (t1.isEmpty) {
        results += t0
        seen += t0
      } else {
        val t2 = t1.filterNot(IsomorphismTester.containsUpToIsomorphism(seen.toStream))
        seen ++ t2
        worklist ++= t2
      }
    }

    results.toStream
  }

  def executeProgram[LV, LE](host: Graph[LV, LE])(program: List[Set[Rule[LV, LE]]]): List[Graph[LV, LE]] =
    program.foldLeft(List[Graph[LV, LE]](host)){ case (hosts, rules) => hosts.flatMap(applyUntilNormal(_)(rules)) }
}
