package lib.core.util

import lib.core.common.{E, Function, Graph, V}

object SubgraphFinder {

  // this generates all subgraphs isomorphic to the search graph
  def find[LV, LE](host: Graph[LV, LE])(search: Graph[LV, LE]): Iterable[Graph[LV, LE]] =
    host.vs.subsets(search.vs.size).toStream.flatMap {
      vs => getIncidentEdges(host)(vs).subsets(search.es.size).flatMap {
        es => GraphBuilder.build(vs, es, host.s, host.t, host.l, host.m)
          .map(Iterator.single)
          .getOrElse(Iterator.empty)
      }
    }

  // this generates all possible subgraphs
  def findAll[LV, LE](host: Graph[LV, LE]): Iterable[Graph[LV, LE]] =
    host.vs.subsets().toStream.flatMap {
      vs => getIncidentEdges(host)(vs).subsets().flatMap {
        es => GraphBuilder.build(vs, es, host.s, host.t, host.l, host.m)
          .map(Iterator.single)
          .getOrElse(Iterator.empty)
      }
    }

  private def getIncidentEdges[LV, LE](host: Graph[LV, LE])(vs: Set[V]): Set[E] =
    Function.preimage(host.s)(vs) union Function.preimage(host.t)(vs)

  private object GraphBuilder {
    def build[LV, LE](vs: Set[V], es: Set[E], s: Function[E, V], t: Function[E, V], l: Function[V, LV], m: Function[E, LE]): Option[Graph[LV, LE]] =
      Some(Graph(vs, es, Function.restrict(s)(es), Function.restrict(t)(es), Function.restrict(l)(vs), Function.restrict(m)(es)))
        .filter(hasClosedSourceFunction)
        .filter(hasClosedTargetFunction)

    private def hasClosedSourceFunction[LV, LE](graph: Graph[LV, LE]): Boolean =
      hasClosedEdgeFunction(graph.vs, graph.es, graph.s)

    private def hasClosedTargetFunction[LV, LE](graph: Graph[LV, LE]): Boolean =
      hasClosedEdgeFunction(graph.vs, graph.es, graph.t)

    private def hasClosedEdgeFunction[LV, LE](vs: Set[V], es: Set[E], f: Function[E, V]): Boolean =
      es.forall(e => vs.contains(Function.eval(f)(e)))
  }
}
