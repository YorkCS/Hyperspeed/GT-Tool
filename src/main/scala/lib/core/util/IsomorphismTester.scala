package lib.core.util

import lib.core.common.{Function, Graph, Morphism}

object IsomorphismTester {

  def isIsomorphic[LV, LE](a: Graph[LV, LE])(b: Graph[LV, LE]): Boolean =
    computeIsomorphisms(a)(b).nonEmpty

  def containsUpToIsomorphism[LV, LE](as: Iterable[Graph[LV, LE]])(a: Graph[LV, LE]): Boolean =
    as.exists(IsomorphismTester.isIsomorphic(a))

  def upToIsomorphism[LV, LE](as: Iterable[Graph[LV, LE]]): Iterable[Graph[LV, LE]] =
    as.foldLeft(List.empty[Graph[LV, LE]]){
      case (results, graph) => if (containsUpToIsomorphism(results)(graph)) results else graph :: results
    }

  def computeIsomorphisms[LV, LE](a: Graph[LV, LE])(b: Graph[LV, LE]): Iterable[Morphism] =
    computePreIsomorphisms(a)(b)
      .filter(isVertexLabelPreserving(a)(b))
      .filter(isEdgeLabelPreserving(a)(b))

  def computePreIsomorphisms[LV, LE](a: Graph[LV, LE])(b: Graph[LV, LE]): Iterable[Morphism] =
    candidateBijections(a)(b)
      .filter(isSourcePreserving(a)(b))
      .filter(isTargetPreserving(a)(b))

  private def candidateBijections[LV, LE](left: Graph[LV, LE])(image: Graph[LV, LE]): Iterable[Morphism] =
    if (left.vs.size == image.vs.size && left.es.size == image.es.size) {
      setBijections(left.vs.toSeq)(image.vs.toSeq).flatMap {
        v => setBijections(left.es.toSeq)(image.es.toSeq)
          .map(e => Morphism(v, e))
      }
    } else {
      Iterable.empty
    }

  private def setBijections[A](xs: Seq[A])(ys: Seq[A]): Iterable[Set[(A, A)]] =
    ys.permutations.toStream.map(zs => (xs zip zs).toSet)

  private def isSourcePreserving[LV, LE](left: Graph[LV, LE])(image: Graph[LV, LE])(g: Morphism): Boolean =
    left.es.forall(e => Function.eval(Function.compose(left.s, g.v))(e) == Function.eval(Function.compose(g.e, image.s))(e))

  private def isTargetPreserving[LV, LE](left: Graph[LV, LE])(image: Graph[LV, LE])(g: Morphism): Boolean =
    left.es.forall(e => Function.eval(Function.compose(left.t, g.v))(e) == Function.eval(Function.compose(g.e, image.t))(e))

  // in general, label preserving morphisms need not preserve undefinedness, however, isomorphisms need to
  private def isVertexLabelPreserving[LV, LE](left: Graph[LV, LE])(image: Graph[LV, LE])(g: Morphism): Boolean =
    left.vs.forall(v => Function.evalSafe(left.l)(v) == Function.evalSafe(Function.compose(g.v, image.l))(v))

  private def isEdgeLabelPreserving[LV, LE](left: Graph[LV, LE])(image: Graph[LV, LE])(g: Morphism): Boolean =
    left.es.forall(e => Function.eval(left.m)(e) == Function.eval(Function.compose(g.e, image.m))(e))
}
