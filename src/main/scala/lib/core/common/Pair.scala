package lib.core.common

case class Pair[LV, LE](G1: Graph[LV, LE], g1: Morphism, G2: Graph[LV, LE], g2: Morphism, G3: Graph[LV, LE])
