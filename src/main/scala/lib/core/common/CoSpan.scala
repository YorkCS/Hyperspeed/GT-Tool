package lib.core.common

case class CoSpan[LV, LE](G: Graph[LV, LE], l: (Graph[LV, LE], Morphism), r: (Graph[LV, LE], Morphism))
