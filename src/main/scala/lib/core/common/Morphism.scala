package lib.core.common

case class Morphism(v: Function[V, V], e: Function[E, E])

object Morphism {
  def eval[LV, LE](f: Morphism)(graph: Graph[LV, LE]): Graph[LV, LE] = {
    val vs = Function.image(f.v)(graph.vs)
    val es = Function.image(f.e)(graph.es)
    val s = graph.s.flatMap { case (e, v) => Function.evalSafe(f.e)(e).flatMap(ee => Function.evalSafe(f.v)(v).map(vv => (ee, vv))) }
    val t = graph.t.flatMap { case (e, v) => Function.evalSafe(f.e)(e).flatMap(ee => Function.evalSafe(f.v)(v).map(vv => (ee, vv))) }
    val l = graph.l.flatMap { case (v, ll) => Function.evalSafe(f.v)(v).map(vv => (vv, ll)) }
    val m = graph.m.flatMap { case (e, mm) => Function.evalSafe(f.e)(e).map(ee => (ee, mm)) }
    Graph(vs, es, s, t, l, m)
  }

  def compose(f: Morphism, g: Morphism): Morphism =
    Morphism(Function.compose(f.v, g.v), Function.compose(f.e, g.e))

  def id[LV, LE](graph: Graph[LV, LE]): Morphism =
    Morphism(Function.id(graph.vs), Function.id(graph.es))

  def overrd(f: Morphism, g: Morphism): Morphism =
    Morphism(Function.overrd(f.v, g.v), Function.overrd(f.e, g.e))

  def inverse(f: Morphism): Morphism =
    Morphism(Function.inverse(f.v), Function.inverse(f.e))
}
