package lib.core.common

import scala.language.implicitConversions

case class Function[A, B](value: Set[(A, B)])

object Function {

  implicit def functionToSet[A, B](f: Function[A, B]): Set[(A, B)] = f.value

  implicit def setToFunction[A, B](s: Set[(A, B)]): Function[A, B] = Function(s)

  def empty[A, B]: Function[A, B] = Set.empty[(A, B)]

  def imageF[A, B](f: Function[A, B]): Set[B] =
    f.map(_._2)

  def image[A, B](f: Function[A, B])(x: Set[A]): Set[B] =
    imageF(restrict(f)(x))

  def evalSafe[A, B](f: Function[A, B])(x: A): Option[B] =
    image(f)(Set(x)).headOption

  def eval[A, B](f: Function[A, B])(x: A): B =
    evalSafe(f)(x).get

  def preimageF[A, B](f: Function[A, B]): Set[A] =
    f.map(_._1)

  def preimage[A, B](f: Function[A, B])(y: Set[B]): Set[A] =
    preimageF(corestrict(f)(y))

  def coevalSafe[A, B](f: Function[A, B])(y: B): Option[A] =
    preimage(f)(Set(y)).headOption

  def coeval[A, B](f: Function[A, B])(y: B): A =
    coevalSafe(f)(y).get

  def restrict[A, B](f: Function[A, B])(x: Set[A]): Function[A, B] =
    f.filter{
      case (a, _) => x.contains(a)
    }

  def antirestrict[A, B](f: Function[A, B])(x: Set[A]): Function[A, B] =
    f.filter{
      case (a, _) => !x.contains(a)
    }

  def corestrict[A, B](f: Function[A, B])(arg: Set[B]): Function[A, B] =
    f.filter{
      case (_, b) => arg.contains(b)
    }

  def coantirestrict[A, B](f: Function[A, B])(arg: Set[B]): Function[A, B] =
    f.filter{
      case (_, b) => !arg.contains(b)
    }

  def compose[A, B, C](f: Function[A, B], g: Function[B, C]): Function[A, C] =
    f.flatMap{ case (a, b) => image(g)(Set(b)).map((a, _)) }

  def id[A](a: Set[A]): Function[A, A] =
    a.map(x => (x, x))

  def const[A, B](a: Set[A])(b: B): Function[A, B] =
    a.map(x => (x, b))

  def overrd[A, B](f: Function[A, B], g: Function[A, B]): Function[A, B] =
    g union antirestrict(f)(preimageF(g))

  def inverse[A, B](f:Function[A, B]): Function[B, A] =
    f.map{ case (x, y) => (y, x)}
}
