package lib.core.common

case class Span[LV, LE](G: Graph[LV, LE], l: (Morphism, Graph[LV, LE]), r: (Morphism, Graph[LV, LE]))
