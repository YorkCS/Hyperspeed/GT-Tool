package lib.core.common

object Printer {

  def printGraph[LV, LE](graph: Graph[LV, LE]): String =
    "[ " ++ printNodes(graph) ++ " | " ++ printEdges(graph) ++ " ]"

  def printNodes[LV, LE](graph: Graph[LV, LE]): String =
    graph.vs.map(printNode(graph)).mkString(" ")

  def printNode[LV, LE](graph: Graph[LV, LE])(v: V): String = {
    val id = v.value.toString()
    val label = Function.evalSafe(graph.l)(v).map(l => "\"" ++ l.toString ++ "\"").getOrElse("empty")
    "(" ++ id ++ ", " ++ label ++ ")"
  }

  def printEdges[LV, LE](graph: Graph[LV, LE]): String =
    graph.es.map(printEdge(graph)).mkString(" ")

  def printEdge[LV, LE](graph: Graph[LV, LE])(e: E): String = {
    val id = e.value.toString()
    val source = Function.eval(graph.s)(e).value.toString()
    val target = Function.eval(graph.t)(e).value.toString()
    val label = Function.evalSafe(graph.m)(e).map(m => "\"" ++ m.toString ++ "\"").getOrElse("empty")
    "(" ++ id ++ ", " ++ source ++ ", " ++ target ++ ", " ++ label ++ ")"
  }
}
