package lib.core.common

case class Rule[LV, LE](name: String, l: Graph[LV, LE], k: Graph[LV, LE], r: Graph[LV, LE])
