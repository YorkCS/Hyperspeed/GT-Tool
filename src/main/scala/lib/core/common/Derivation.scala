package lib.core.common

case class Derivation[LV, LE](r: Rule[LV, LE], g: Morphism)
