package lib.core.common

case class Graph[LV, LE](vs: Set[V], es: Set[E], s: Function[E, V], t: Function[E, V], l: Function[V, LV], m: Function[E, LE])

object Graph {
  def discrete[LV, LE](l: Function[V, LV]): Graph[LV, LE] =
    Graph(l.map(_._1), Set.empty[E], Function.empty[E, V], Function.empty[E, V], l, Function.empty[E, LE])

  def empty[LV, LE]: Graph[LV, LE] =
    discrete[LV, LE](Function.empty[V, LV])

  def unlabelled(vs: Set[V], es: Set[E], s: Function[E, V], t: Function[E, V]): Graph[Unit, Unit] =
    Graph(vs, es, s, t, vs.map((_, ())), es.map((_, ())))

  def subgraph[LV, LE](graph: Graph[LV, LE])(vs: Set[V])(es: Set[E]): Graph[LV, LE] =
    Graph(vs, es, Function.restrict(graph.s)(es), Function.restrict(graph.t)(es), Function.restrict(graph.l)(vs), Function.restrict(graph.m)(es))

  def removeLabels[LV, LE](graph: Graph[LV, LE])(ls: Set[V]): Graph[LV, LE] =
    Graph(graph.vs, graph.es, graph.s, graph.t, Function.antirestrict(graph.l)(ls), graph.m)

  def outgoing[LV, LE](graph: Graph[LV, LE])(v: V): Set[E] =
    Function.preimage(graph.s)(Set(v))

  def incoming[LV, LE](graph: Graph[LV, LE])(v: V): Set[E] =
    Function.preimage(graph.t)(Set(v))
}
