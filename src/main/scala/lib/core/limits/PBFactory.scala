package lib.core.limits

import lib.core.common.{CoSpan, Function, Graph, Morphism, Span, V}

object PBFactory {

  // expects all morphisms to be injective
  def build[LV, LE](cospan: CoSpan[LV, LE]): Span[LV, LE] = {
    val vs = Function.preimage(cospan.l._2.v)(Function.imageF(cospan.r._2.v))
    val es = Function.preimage(cospan.l._2.e)(Function.imageF(cospan.r._2.e))
    val graph = Graph.subgraph(cospan.l._1)(vs)(es)
    Span(
      graph,
      (Morphism.id(graph), cospan.l._1),
      (Morphism.compose(Morphism.id(graph), Morphism.compose(cospan.l._2, Morphism.inverse(cospan.r._2))), cospan.r._1)
    )
  }
}
