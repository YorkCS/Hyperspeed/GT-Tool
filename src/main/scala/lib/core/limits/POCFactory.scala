package lib.core.limits

import lib.core.common.{Function, Graph, Morphism, Pair}

object POCFactory {

  // expects all morphisms to be injective and the
  // second morphism to satisfy the dangling condition
  // first graph may be partially labelled, but the others totally
  def build[LV, LE](pair: Pair[LV, LE]): Pair[LV, LE] = {
    val vs = pair.G3.vs diff Function.image(pair.g2.v)(pair.G2.vs diff Function.preimageF(pair.g1.v))
    val es = pair.G3.es diff Function.image(pair.g2.e)(pair.G2.es diff Function.preimageF(pair.g1.e))
    val ls = Function.image(pair.g2.v)(Function.preimageF(pair.g1.v) diff Function.preimage(pair.g1.v)(Function.preimageF(pair.G1.l)))
    val graph = Graph.removeLabels(Graph.subgraph(pair.G3)(vs)(es))(ls)
    Pair(
      pair.G1,
      Morphism(Function.restrict(pair.g2.v)(Function.preimageF(pair.g1.v)), Function.restrict(pair.g2.e)(Function.preimageF(pair.g1.e))),
      graph,
      Morphism.id(graph),
      pair.G3
    )
  }
}
