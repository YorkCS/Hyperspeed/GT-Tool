package lib.core.limits

import lib.core.common.{CoSpan, E, Function, Graph, Morphism, Span, V}

object POFactory {

  // expects all morphisms to be injective
  def build[LV, LE](span: Span[LV, LE]): CoSpan[LV, LE] =
    CoSpan(
      buildGraph(span),
      (span.l._2, buildLeftMorphism(span)),
      (span.r._2, buildRightMorphism(span))
    )

  // expects all morphisms to be injective
  def buildGraph[LV, LE](span: Span[LV, LE]): Graph[LV, LE] = {
    val vs = span.l._2.vs.map(ResultGraphBuilder.mapV(true)) union (span.r._2.vs diff Function.imageF(span.r._1.v)).map(ResultGraphBuilder.mapV(false))
    val es = span.l._2.es.map(ResultGraphBuilder.mapE(true)) union (span.r._2.es diff Function.imageF(span.r._1.e)).map(ResultGraphBuilder.mapE(false))
    val s = es.map(e => (e, ResultGraphBuilder.computeSourceVertex(span)(e)))
    val t = es.map(e => (e, ResultGraphBuilder.computeTargetVertex(span)(e)))
    val l = vs.map(v => (v, ResultGraphBuilder.computeVertexLabel(span)(v)))
    val m = es.map(e => (e, ResultGraphBuilder.computeEdgeLabel(span)(e)))
    Graph(vs, es, s, t, l, m)
  }

  private def buildLeftMorphism[LV, LE](span: Span[LV, LE]): Morphism =
    Morphism(
      span.l._2.vs.map(v => (v, ResultGraphBuilder.mapV(true)(v))),
      span.l._2.es.map(e => (e, ResultGraphBuilder.mapE(true)(e))),
    )

  private def buildRightMorphism[LV, LE](span: Span[LV, LE]): Morphism =
    Morphism(
      span.r._1.v.map{ case (s, t) => (t, ResultGraphBuilder.mapV(true)(Function.eval(span.l._1.v)(s)))} union (span.r._2.vs diff Function.imageF(span.r._1.v)).map(v => (v, ResultGraphBuilder.mapV(false)(v))),
      span.r._1.e.map{ case (s, t) => (t, ResultGraphBuilder.mapE(true)(Function.eval(span.l._1.e)(s)))} union (span.r._2.es diff Function.imageF(span.r._1.e)).map(e => (e, ResultGraphBuilder.mapE(false)(e))),
    )

  private object ResultGraphBuilder {

    private def map(b: Boolean)(i: BigInt): BigInt =
      2 * i + (if (b) 0 else 1)

    def mapE(b: Boolean)(e: E): E =
      E(map(b)(e.value))

    def mapV(b: Boolean)(v: V): V =
      V(map(b)(v.value))

    private def unmap(i: BigInt): Either[BigInt, BigInt] =
      if (i % 2 == 0) Left(i / 2) else Right(i / 2)

    private def unmapE(e: E): Either[E, E] =
      unmap(e.value) match {
        case Left(i) => Left(E(i))
        case Right(i) => Right(E(i))
      }

    private def unmapV(v: V): Either[V, V] =
      unmap(v.value) match {
        case Left(i) => Left(V(i))
        case Right(i) => Right(V(i))
      }

    def computeSourceVertex[LV, LE](span: Span[LV, LE])(e: E): V = {
      unmapE(e) match {
        case Left(e0) => mapV(true)(Function.eval(span.l._2.s)(e0))
        case Right(e0) =>
          val v = Function.eval(span.r._2.s)(e0)
          Function.evalSafe(Function.compose(Function.inverse(span.r._1.v), span.l._1.v))(v)
            .map(mapV(true))
            .getOrElse(mapV(false)(v))
      }
    }

    def computeTargetVertex[LV, LE](span: Span[LV, LE])(e: E): V = {
      unmapE(e) match {
        case Left(e0) => mapV(true)(Function.eval(span.l._2.t)(e0))
        case Right(e0) =>
          val v = Function.eval(span.r._2.t)(e0)
          Function.evalSafe(Function.compose(Function.inverse(span.r._1.v), span.l._1.v))(v)
            .map(mapV(true))
            .getOrElse(mapV(false)(v))
      }
    }

    def computeVertexLabel[LV, LE](span: Span[LV, LE])(v: V): LV =
      unmapV(v) match {
        case Left(v0) => Function.evalSafe(span.l._2.l)(v0).getOrElse(Function.eval(span.r._2.l)(Function.coeval(span.l._1.v)(v0)))
        case Right(v0) => Function.eval(span.r._2.l)(v0)
      }

    def computeEdgeLabel[LV, LE](span: Span[LV, LE])(e: E): LE =
      unmapE(e) match {
        case Left(e0) => Function.eval(span.l._2.m)(e0)
        case Right(e0) => Function.eval(span.r._2.m)(e0)
      }
  }
}
