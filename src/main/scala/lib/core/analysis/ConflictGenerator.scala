package lib.core.analysis

import lib.core.common.{CoSpan, Function, Graph, Morphism, Rule, Span}
import lib.core.limits.{PBFactory, POFactory}

object ConflictGenerator {

  // generates the conflict reason span, if there is a conflict
  def generateConflictReason[LV, LE](pair: CriticalPair[LV, LE]): Option[Span[LV, LE]] = {
    val conflicts = generateConflictPair(pair)
    if (conflicts._1._2) {
      if (conflicts._2._2) {
        val S = POFactory.build(
          PBFactory.build(
            CoSpan(
              pair.s,
              (conflicts._1._1.G, Morphism.compose(conflicts._1._1.l._1, pair.l.g)),
              (conflicts._2._1.G, Morphism.compose(conflicts._2._1.l._1, pair.r.g))
            )
          )
        )
        Some(
          Span(
            S.G,
            (Morphism.overrd(Morphism.compose(Morphism.inverse(S.l._2), conflicts._1._1.l._1), Morphism.compose(Morphism.inverse(S.r._2), conflicts._2._1.r._1)), pair.l.r.l),
            (Morphism.overrd(Morphism.compose(Morphism.inverse(S.r._2), conflicts._2._1.l._1), Morphism.compose(Morphism.inverse(S.l._2), conflicts._1._1.r._1)), pair.r.r.l)
          )
        )
      } else {
        Some(
          Span(
            conflicts._1._1.G,
            (conflicts._1._1.l._1, pair.l.r.l),
            conflicts._1._1.r
          )
        )
      }
    } else {
      if (conflicts._2._2) {
        Some(
          Span(
            conflicts._2._1.G,
            conflicts._2._1.r,
            (conflicts._2._1.l._1, pair.r.r.l),
          )
        )
      } else {
        Option.empty
      }
    }
  }

  // generates the pair of conflict spans and indicates if they are in conflict
  def generateConflictPair[LV, LE](pair: CriticalPair[LV, LE]): ((Span[LV, LE], Boolean), (Span[LV, LE], Boolean)) = {
    val C1 = generateContextGraph(pair.l.r)
    val C2 = generateContextGraph(pair.r.r)
    val S1 = PBFactory.build(CoSpan(pair.s, (C1, Morphism.compose(Morphism.id(C1), pair.l.g)), (pair.r.r.l, pair.r.g)))
    val S2 = PBFactory.build(CoSpan(pair.s, (C2, Morphism.compose(Morphism.id(C2), pair.r.g)), (pair.l.r.l, pair.l.g)))
    (
      (S1, !isContainedInBoundary(pair.l.r)(Morphism.eval(S1.l._1)(S1.G))),
      (S2, !isContainedInBoundary(pair.r.r)(Morphism.eval(S2.l._1)(S2.G)))
    )
  }

  private def generateContextGraph[LV, LE](rule: Rule[LV, LE]): Graph[LV, LE] = {
    val vs = rule.l.vs diff rule.k.vs
    val es = rule.l.es diff rule.k.es
    Graph.subgraph(rule.l)(vs union Function.image(rule.l.s)(es) union Function.image(rule.l.t)(es))(es)
  }

  private def isContainedInBoundary[LV, LE](rule: Rule[LV, LE])(graph: Graph[LV, LE]): Boolean = {
    val es = rule.l.es diff rule.k.es
    graph.vs.subsetOf((Function.image(rule.l.s)(es) union Function.image(rule.l.t)(es)) intersect rule.k.vs) && graph.es.isEmpty
  }
}
