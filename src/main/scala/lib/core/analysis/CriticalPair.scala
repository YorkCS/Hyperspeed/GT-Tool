package lib.core.analysis

import lib.core.common.{Derivation, Graph}

case class CriticalPair[LV, LE](s: Graph[LV, LE], l: Derivation[LV, LE], r: Derivation[LV, LE])
