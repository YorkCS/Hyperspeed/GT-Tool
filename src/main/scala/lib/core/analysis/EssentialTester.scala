package lib.core.analysis

import lib.core.common.{CoSpan, Morphism, Span}
import lib.core.limits.POFactory
import lib.core.util.IsomorphismTester

object EssentialTester {

  def isEssential[LV, LE](pair: CriticalPair[LV, LE]): Boolean =
    ConflictGenerator.generateConflictReason(pair)
      .exists(isConflictEssential(CoSpan(pair.s, (pair.l.r.l, pair.l.g), (pair.r.r.l, pair.r.g))))

  private def isConflictEssential[LV, LE](matches: CoSpan[LV, LE])(conflict: Span[LV, LE]): Boolean =
    areCoSpansIsomorphic(POFactory.build(conflict))(matches)

  // given two cospans with the same top two graphs, are they isomorphic?
  private def areCoSpansIsomorphic[LV, LE](a: CoSpan[LV, LE])(b: CoSpan[LV, LE]): Boolean =
    IsomorphismTester.computeIsomorphisms(a.G)(b.G).exists(
      f => (Morphism.compose(a.l._2, f) == b.l._2) && (Morphism.compose(a.r._2, f) == b.r._2)
    )
}
