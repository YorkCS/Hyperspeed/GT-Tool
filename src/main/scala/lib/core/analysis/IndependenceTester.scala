package lib.core.analysis

import lib.core.common.{Morphism, Rule}

object IndependenceTester {

  // this is only correct when rule interfaces have total node labelling
  // this does not check for bad input: the morphisms better be valid matches
  def isIndependent[LV, LE](r1: Rule[LV, LE], g1: Morphism)(r2: Rule[LV, LE], g2: Morphism): Boolean = {
    val l1 = Morphism.eval(g1)(r1.l)
    val k1 = Morphism.eval(g1)(r1.k)
    val l2 = Morphism.eval(g2)(r2.l)
    val k2 = Morphism.eval(g2)(r2.k)
    (l1.vs intersect l2.vs).subsetOf(k1.vs intersect k2.vs) && (l1.es intersect l2.es).subsetOf(k1.es intersect l2.es)
  }
}
