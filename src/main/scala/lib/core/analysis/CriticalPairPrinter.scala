package lib.core.analysis

import lib.core.common.{Printer, Rule}

object CriticalPairPrinter {

  def printCriticalPair[LV, LE](pair: CriticalPair[LV, LE]): String =
    "CriticalPair(\n  " ++ Printer.printGraph(pair.s) ++ ",\n  Derivation(" ++ pair.l.r.name ++ ", " ++ pair.l.g.toString ++ "),\n  Derivation(" ++ pair.r.r.name ++ ", " ++ pair.r.g.toString ++ ")\n)"

  def printEssentialStatus[LV, LE](pair: CriticalPair[LV, LE]): String = {
    "ESSENTIAL: " ++ (if (EssentialTester.isEssential(pair)) "TRUE" else "FALSE")
  }

  def printJoinabilityStatus[LV, LE](rules: Set[Rule[LV, LE]])(pair: CriticalPair[LV, LE]): String =
    "JOINABLE: " ++ (if (JoinabilityTester.isJoinable(rules)(pair)) "TRUE" else "FALSE")

  def printSubcommutativityStatus[LV, LE](rules: Set[Rule[LV, LE]])(pair: CriticalPair[LV, LE]): String =
    "SUBCOMMUTATIVE: " ++ (if (JoinabilityTester.isSubcommutative(rules)(pair)) "TRUE" else "FALSE")

  def printStrongJoinabilityStatus[LV, LE](rules: Set[Rule[LV, LE]])(pair: CriticalPair[LV, LE]): String =
    "STRONGLY JOINABLE: " ++ (if (JoinabilityTester.isStronglyJoinable(rules)(pair)) "TRUE" else "FALSE")

  def printStrongSubcommutativityStatus[LV, LE](rules: Set[Rule[LV, LE]])(pair: CriticalPair[LV, LE]): String =
    "STRONGLY SUBCOMMUTATIVE: " ++ (if (JoinabilityTester.isStronglySubcommutative(rules)(pair)) "TRUE" else "FALSE")

  def printConflictReason[LV, LE](pair: CriticalPair[LV, LE]): String = {
    val reason = ConflictGenerator.generateConflictPair(pair)
    "CONFLICT REASON: " ++ (if (reason._1._2 && !reason._2._2) "delete-use conflict" else if (!reason._1._2 && reason._2._2) "use-delete conflict" else if (!reason._1._2 && !reason._2._2) "not in conflict" else "symmetric conflict")
  }
}
