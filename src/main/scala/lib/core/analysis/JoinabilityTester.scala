package lib.core.analysis

import lib.core.common.{Function, Graph, Morphism, Rule, V}
import lib.core.matching.MatchApplier
import lib.core.rewriting.GTSystem
import lib.core.util.IsomorphismTester

import scala.collection.mutable

object JoinabilityTester {

  def isJoinable[LV, LE](rules: Set[Rule[LV, LE]])(pair: CriticalPair[LV, LE]): Boolean = {
    val lt = generateResult(pair.s)(pair.l.r)(pair.l.g)
    val ls = generateAllDescendants(rules)(lt)
    val rt = generateResult(pair.s)(pair.r.r)(pair.r.g)
    val rs = generateAllDescendants(rules)(rt)
    ls.exists(l => rs.exists(r => IsomorphismTester.isIsomorphic(l._1)(r._1)))
  }

  def isSubcommutative[LV, LE](rules: Set[Rule[LV, LE]])(pair: CriticalPair[LV, LE]): Boolean = {
    val lt = generateResult(pair.s)(pair.l.r)(pair.l.g)
    val ls = generateAllDirectDescendants(rules)(lt)
    val rt = generateResult(pair.s)(pair.r.r)(pair.r.g)
    val rs = generateAllDirectDescendants(rules)(rt)
    ls.exists(l => rs.exists(r => IsomorphismTester.isIsomorphic(l._1)(r._1)))
  }

  def isStronglyJoinable[LV, LE](rules: Set[Rule[LV, LE]])(pair: CriticalPair[LV, LE]): Boolean = {
    val lt = generateResult(pair.s)(pair.l.r)(pair.l.g)
    val ls = generateAllDescendants(rules)(lt)
    val rt = generateResult(pair.s)(pair.r.r)(pair.r.g)
    val rs = generateAllDescendants(rules)(rt)
    val ps = generatePersist(lt._2)(rt._2)
    ls.exists(l => rs.exists(r => IsomorphismTester.computeIsomorphisms(l._1)(r._1).exists(g => testPersist(ps)(Function.compose(l._2, g.v))(r._2))))
  }

  def isStronglySubcommutative[LV, LE](rules: Set[Rule[LV, LE]])(pair: CriticalPair[LV, LE]): Boolean = {
    val lt = generateResult(pair.s)(pair.l.r)(pair.l.g)
    val ls = generateAllDirectDescendants(rules)(lt)
    val rt = generateResult(pair.s)(pair.r.r)(pair.r.g)
    val rs = generateAllDirectDescendants(rules)(rt)
    val ps = generatePersist(lt._2)(rt._2)
    ls.exists(l => rs.exists(r => IsomorphismTester.computeIsomorphisms(l._1)(r._1).exists(g => testPersist(ps)(Function.compose(l._2, g.v))(r._2))))
  }

  private def generatePersist[LV, LE](t0: Function[V, V])(t1: Function[V, V]): Set[V] =
    Function.preimageF(t0) intersect Function.preimageF(t1)

  private def testPersist[LV, LE](ps: Set[V])(t0: Function[V, V])(t1: Function[V, V]): Boolean = {
    val rt0 = Function.restrict(t0)(ps)
    val rt1 = Function.restrict(t1)(ps)
    (Function.preimageF(rt0) == ps) && (Function.preimageF(rt1) == ps) && (rt0 == rt1)
  }

  private def generateResult[LV, LE](host: Graph[LV, LE])(rule: Rule[LV, LE])(g: Morphism): (Graph[LV, LE], Function[V, V]) = {
    val result = MatchApplier.build(host)(rule)(g)
    (result, MatchApplier.track(result).v)
  }

  private def generateAllDirectDescendants[LV, LE](rules: Set[Rule[LV, LE]])(input: (Graph[LV, LE],  Function[V, V])): List[(Graph[LV, LE], Function[V, V])] = {
    List(input) ++ generateAllChildren(rules)(input._1, input._2)
  }

  private def generateAllDescendants[LV, LE](rules: Set[Rule[LV, LE]])(input: (Graph[LV, LE],  Function[V, V])): List[(Graph[LV, LE], Function[V, V])] = {
    val results = mutable.Set(input)
    val worklist = mutable.Queue(input)

    while (worklist.nonEmpty) {
      val t = worklist.dequeue()
      val ts = generateAllChildren(rules)(t._1, t._2)
      results ++= ts
      worklist ++= ts
    }

    results.iterator.toList
  }

  private def generateAllChildren[LV, LE](rules: Set[Rule[LV, LE]])(host: Graph[LV, LE], track: Function[V, V]): List[(Graph[LV, LE], Function[V, V])] = {
    rules.iterator.flatMap(rule => GTSystem.applyOnceRaw(host)(rule).map(result => (result, Function.compose(track, MatchApplier.track(result).v)))).toList
  }
}
