package lib.core.analysis

import lib.core.analysis
import lib.core.common.{Derivation, E, Function, Graph, Morphism, Rule, V}
import lib.core.matching.MatchFinder
import lib.core.util.{IsomorphismTester, SubgraphFinder}

object CriticalPairGenerator {

  def generateCriticalPairs[LV, LE](rules: Set[Rule[LV, LE]]): List[CriticalPair[LV, LE]] =
    generateRulePairs(rules)
      .flatMap { case (r0, r1) => generateOverlaps(r0)(r1).map{ case (g, f0, f1) => analysis.CriticalPair(g, Derivation(r0, f0), Derivation(r1, f1)) } }

  private def generateRulePairs[LV, LE](rules: Set[Rule[LV, LE]]): List[(Rule[LV, LE], Rule[LV, LE])] = {
    val ones = rules.subsets(1).map(s => (s.head, s.head))
    val twos = rules.subsets(2).map(s => {
      val a = s.head
      val b = (s diff Set(a)).head
      if (a.name < b.name) (a, b) else (b, a)
    })
    (ones ++ twos).toList.sortWith((p1, p2) => (p1._1.name < p2._1.name) || ((p1._1.name == p2._1.name) && (p1._2.name < p2._2.name)))
  }

  private def generateOverlaps[LV, LE](r0: Rule[LV, LE])(r1: Rule[LV, LE]): List[(Graph[LV, LE], Morphism, Morphism)] =
    generateGenuineOverlaps(r0)(r1).foldLeft(List.empty[(Graph[LV, LE], Morphism, Morphism)]) {
      case (results, overlap) => if (containsUpToIsomorphism(r0)(r1)(results)(overlap)) results else overlap :: results
    }

  private def containsUpToIsomorphism[LV, LE](r0: Rule[LV, LE])(r1: Rule[LV, LE])(as: Iterable[(Graph[LV, LE], Morphism, Morphism)])(b: (Graph[LV, LE], Morphism, Morphism)): Boolean =
    as.exists(a => IsomorphismTester.computeIsomorphisms(a._1)(b._1).exists(f => {
      ((Morphism.compose(a._2, f) == b._2) && (Morphism.compose(a._3, f) == b._3)) || (r0 == r1 && (Morphism.compose(a._2, f) == b._3) && (Morphism.compose(a._2, f) == b._3))
    }))

  private def generateGenuineOverlaps[LV, LE](r0: Rule[LV, LE])(r1: Rule[LV, LE]): List[(Graph[LV, LE], Morphism, Morphism)] =
    generatePotentialOverlaps(r0)(r1)
      .filterNot { case (_, f0, f1) => IndependenceTester.isIndependent(r0, f0)(r1, f1) }
      .filter { case (_, f0, f1) => r0 != r1 || f0 != f1 }
      .toSet
      .toList

  private def generatePotentialOverlaps[LV, LE](r0: Rule[LV, LE])(r1: Rule[LV, LE]): Iterator[(Graph[LV, LE], Morphism, Morphism)] =
    generatePotentialOverlapMorphisms(r0.l)(r1.l)
      .map(stickTogether(r0.l)(r1.l))
      .filter{ case (g, f, _) => MatchFinder.satisfiesDanglingCondition(r0)(g)(f) }
      .filter{ case (g, _, f) => MatchFinder.satisfiesDanglingCondition(r1)(g)(f) }

  private def generatePotentialOverlapMorphisms[LV, LE](l0: Graph[LV, LE])(l1: Graph[LV, LE]): Iterator[Morphism] = {
    val s0 = SubgraphFinder.findAll(l0).toSet
    val s1 = SubgraphFinder.findAll(l1).toSet
    s0.flatMap(g => s1.flatMap(h => IsomorphismTester.computeIsomorphisms(g)(h))).iterator
  }

  private def stickTogether[LV, LE](a: Graph[LV, LE])(b: Graph[LV, LE])(g: Morphism): (Graph[LV, LE], Morphism, Morphism) = {
    val vs = a.vs.map(OverlapGraphBuilder.mapV(true)) union (b.vs diff Function.imageF(g.v)).map(OverlapGraphBuilder.mapV(false))
    val es = a.es.map(OverlapGraphBuilder.mapE(true)) union (b.es diff Function.imageF(g.e)).map(OverlapGraphBuilder.mapE(false))
    val s = es.map(e => (e, OverlapGraphBuilder.computeSourceVertex(a)(b)(g)(e)))
    val t = es.map(e => (e, OverlapGraphBuilder.computeTargetVertex(a)(b)(g)(e)))
    val l = vs.map(v => (v, OverlapGraphBuilder.computeVertexLabel(a)(b)(v)))
    val m = es.map(e => (e, OverlapGraphBuilder.computeEdgeLabel(a)(b)(e)))
    val f0 = OverlapGraphBuilder.computeTargetMapping(Morphism.id(a))(true)
    val f1 = Morphism.overrd(OverlapGraphBuilder.computeTargetMapping(Morphism.id(b))(false), OverlapGraphBuilder.computeTargetMapping(Morphism.inverse(g))(true))
    (Graph(vs, es, s, t, l, m), f0, f1)
  }

  private object OverlapGraphBuilder {

    private def map(b: Boolean)(i: BigInt): BigInt =
      2 * i + (if (b) 0 else 1)

    def mapE(b: Boolean)(e: E): E =
      E(map(b)(e.value))

    def mapV(b: Boolean)(v: V): V =
      V(map(b)(v.value))

    private def unmap(i: BigInt): Either[BigInt, BigInt] =
      if (i % 2 == 0) Left(i / 2) else Right(i / 2)

    private def unmapE(e: E): Either[E, E] =
      unmap(e.value) match {
        case Left(i) => Left(E(i))
        case Right(i) => Right(E(i))
      }

    private def unmapV(v: V): Either[V, V] =
      unmap(v.value) match {
        case Left(i) => Left(V(i))
        case Right(i) => Right(V(i))
      }

    def computeSourceVertex[LV, LE](a: Graph[LV, LE])(b: Graph[LV, LE])(g: Morphism)(e: E): V = {
      unmapE(e) match {
        case Left(e0) => mapV(true)(Function.eval(a.s)(e0))
        case Right(e0) =>
          val v = Function.eval(b.s)(e0)
          Function.coevalSafe(g.v)(v).map(mapV(true)).getOrElse(mapV(false)(v))
      }
    }

    def computeTargetVertex[LV, LE](a: Graph[LV, LE])(b: Graph[LV, LE])(g: Morphism)(e: E): V = {
      unmapE(e) match {
        case Left(e0) => mapV(true)(Function.eval(a.t)(e0))
        case Right(e0) =>
          val v = Function.eval(b.t)(e0)
          Function.coevalSafe(g.v)(v).map(mapV(true)).getOrElse(mapV(false)(v))
      }
    }

    def computeVertexLabel[LV, LE](a: Graph[LV, LE])(b: Graph[LV, LE])(v: V): LV =
      unmapV(v) match {
        case Left(v0) => Function.eval(a.l)(v0)
        case Right(v0) => Function.eval(b.l)(v0)
      }

    def computeEdgeLabel[LV, LE](a: Graph[LV, LE])(b: Graph[LV, LE])(e: E): LE =
      unmapE(e) match {
        case Left(e0) => Function.eval(a.m)(e0)
        case Right(e0) => Function.eval(b.m)(e0)
      }

    def computeTargetMapping[LV, LE](g: Morphism)(b: Boolean): Morphism =
      Morphism(
        g.v.map{ case (x, y) => (x, OverlapGraphBuilder.mapV(b)(y)) },
        g.e.map{ case (x, y) => (x, OverlapGraphBuilder.mapE(b)(y)) }
      )
  }
}
