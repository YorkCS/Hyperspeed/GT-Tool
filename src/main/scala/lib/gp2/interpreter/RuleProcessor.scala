package lib.gp2.interpreter

import lib.core.common.{E, Function, Graph, Morphism, Rule, V}
import lib.gp2.common.{EM, GP2Rule, HostGraph, Ident, Label, RuleGraph, Undashed, Unmarked, VM}

object RuleProcessor {

  def stripRule(rule: GP2Rule): Rule[(List[BigInt], VM), (List[BigInt], EM)] =
    Rule(rule.name, stripGraph(rule.l), stripGraph(rule.k), stripGraph(rule.r))

  def instantiateRule(host: HostGraph)(rule: GP2Rule)(morph: Morphism): Option[Rule[(List[BigInt], VM), (List[BigInt], EM)]] = {
    generateVertexAssignments(host)(rule.l)(morph)
      .flatMap(vass => generateEdgeAssignments(host)(rule.l)(morph).map(eass => (vass, eass)))
      .flatMap {
        case (vass, eass) => instantiateGraph(rule.l)(vass, eass).flatMap(il => instantiateGraph(rule.k)(vass, eass).flatMap(ik => instantiateGraph(rule.r)(vass, eass).map(ir => Rule("", il, ik, ir))))
      }
  }

  def instantiateGraph(graph: RuleGraph)(vass: Map[BigInt, List[BigInt]], eass: Map[BigInt, List[BigInt]]): Option[HostGraph] =
    instantiateVertexLabels(graph)(vass).flatMap(l => instantiateEdgeLabels(graph)(eass).map(m => replaceGraphLabels(graph)(l, m)))

  def instantiateVertexLabels(graph: RuleGraph)(vass: Map[BigInt, List[BigInt]]): Option[Function[V, (List[BigInt], VM)]]=
    graph.l.foldLeft(Option(Function.empty[V, (List[BigInt], VM)])){
      case (None, _) => Option(Function.empty[V, (List[BigInt], VM)])
      case (Some(l), (v, ls)) => ls match {
        case (Ident(i), mark) => vass.get(i).map(j => l union Set((v, (j, mark))))
        case (Label(i), mark) => Option(l union Set((v, (i, mark))))
      }
    }

  def instantiateEdgeLabels(graph: RuleGraph)(vass: Map[BigInt, List[BigInt]]): Option[Function[E, (List[BigInt], EM)]] =
    graph.m.foldLeft(Option(Function.empty[E, (List[BigInt], EM)])){
      case (None, _) => Option(Function.empty[E, (List[BigInt], EM)])
      case (Some(l), (e, ms)) => ms match {
        case (Ident(i), mark) => vass.get(i).map(j => l union Set((e, (j, mark))))
        case (Label(i), mark) => Option(l union Set((e, (i, mark))))
      }
    }

  private def replaceGraphLabels(graph: RuleGraph)(l: Function[V, (List[BigInt], VM)], m: Function[E, (List[BigInt], EM)]): HostGraph =
    Graph(graph.vs, graph.es, graph.s, graph.t, l, m)

  private def stripGraph(graph: RuleGraph): HostGraph =
    replaceGraphLabels(graph)(Function.const(graph.vs)((List.empty[BigInt], Unmarked())), Function.const(graph.es)((List.empty[BigInt], Undashed())))

  private def generateVertexAssignments(host: HostGraph)(left: RuleGraph)(morph: Morphism): Option[Map[BigInt, List[BigInt]]] = {
    left.l.flatMap { case (v, ll) => Function.evalSafe(morph.v)(v).map(vv => (vv, ll)) }.foldLeft(Option(Map.empty[BigInt, List[BigInt]])){
      case (None, _) => Option.empty[Map[BigInt, List[BigInt]]]
      case (Some(ass), (v, ls)) => ls match {
        case (Ident(i), mark) => if (mark != Function.eval(host.l)(v)._2) Option.empty[Map[BigInt, List[BigInt]]] else Option(ass updated (i, Function.eval(host.l)(v)._1))
        case (Label(i), mark) => if ((i, mark) != Function.eval(host.l)(v)) Option.empty[Map[BigInt, List[BigInt]]] else Option(ass)
      }
    }
  }

  private def generateEdgeAssignments(host: HostGraph)(left: RuleGraph)(morph: Morphism): Option[Map[BigInt, List[BigInt]]] = {
    left.m.flatMap { case (e, mm) => Function.evalSafe(morph.e)(e).map(ee => (ee, mm)) }.foldLeft(Option(Map.empty[BigInt, List[BigInt]])){
      case (None, _) => Option.empty[Map[BigInt, List[BigInt]]]
      case (Some(ass), (e, ms)) => ms match {
        case (Ident(i), mark) => if (mark != Function.eval(host.m)(e)._2) Option.empty[Map[BigInt, List[BigInt]]] else Option(ass updated (i, Function.eval(host.m)(e)._1))
        case (Label(i), mark) => if ((i, mark) != Function.eval(host.m)(e)) Option.empty[Map[BigInt, List[BigInt]]] else Option(ass)
      }
    }
  }

}
