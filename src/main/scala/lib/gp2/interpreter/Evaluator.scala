package lib.gp2.interpreter

import lib.core.matching.{MatchApplier, MatchFinder}
import lib.gp2.common._

import scala.annotation.tailrec

object Evaluator {

  @tailrec
  def eval(prog: Program)(host: HostGraph): Option[HostGraph] =
    step(prog)(host) match {
      case Left((p, h)) => eval(p)(h)
      case Right(h) => h
    }

  def step(prog: Program)(host: HostGraph): Either[(Program, HostGraph), Option[HostGraph]] =
    prog match {
      case Sequence(p, q) => step(p)(host) match {
        case Left((s, h)) => Left((Sequence(s, q), h))
        case Right(Some(h)) => Left((q, h))
        case Right(None) => Right(None)
      }
      case RuleSet(r) => Right(applyRuleSet(r)(host))
      case Bang(p) => eval(p)(host) match {
        case Some(h) => Left((Bang(p), h))
        case None => Right(Some(host))
      }
      case IfThen(c, p) => eval(c)(host) match {
        case Some(h) => Left((p, h))
        case None => Right(Some(host))
      }
      case Fail() => Right(Option.empty[HostGraph])
    }

  def applyRuleSet(rules: Set[GP2Rule])(host: HostGraph): Option[HostGraph] =
    rules.toStream.flatMap(rule => applyRule(rule)(host)).headOption

  def applyRule(rule: GP2Rule)(host: HostGraph): Option[HostGraph] =
    MatchFinder.findPreMatches(host)(RuleProcessor.stripRule(rule))
      .flatMap(morph => RuleProcessor.instantiateRule(host)(rule)(morph).map(inst => MatchApplier.build(host)(inst)(morph)))
      .headOption

}
