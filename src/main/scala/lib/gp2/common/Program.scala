package lib.gp2.common

sealed trait Program
case class Sequence(p: Program, q: Program) extends Program
case class RuleSet(r: Set[GP2Rule]) extends Program
case class Bang(p: Program) extends Program
case class IfThen(c: Program, p: Program) extends Program
case class Fail() extends Program
