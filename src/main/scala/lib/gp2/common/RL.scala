package lib.gp2.common

sealed trait RL
case class Ident(i: BigInt) extends RL
case class Label(l: List[BigInt]) extends RL
