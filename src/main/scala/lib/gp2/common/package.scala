package lib.gp2

import lib.core.common.{Graph, Rule}

package object common {
  type HostGraph = Graph[(List[BigInt], VM), (List[BigInt], EM)]
  type RuleGraph = Graph[(RL, VM), (RL, EM)]
  type GP2Rule = Rule[(RL, VM), (RL, EM)]
}
