package lib.gp2.common

sealed trait EM
case class Undashed() extends EM
case class Dashed() extends EM
