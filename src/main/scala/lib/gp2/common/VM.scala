package lib.gp2.common

sealed trait VM
case class Unmarked() extends VM
case class Grey() extends VM
case class Blue() extends VM
case class Green() extends VM
case class Red() extends VM
