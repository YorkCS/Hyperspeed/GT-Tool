name := "GraphTransformation"
version := "0.1"
scalaVersion := "2.12.12"
libraryDependencies ++= Seq(
  "org.scalatest" %% "scalatest-flatspec" % "3.2.3" % "test",
  "org.scalatest" %% "scalatest-mustmatchers" % "3.2.3" % "test"
)
scalacOptions ++= Seq("-deprecation", "-feature")
